BUILDDIR ?= build
OBJECTDIR ?= objects
SOURCEDIR ?= src
INCLUDEDIR ?= include

CPR_BINARYDIR := cpr/build/lib
CPR_INCLUDEDIR := cpr/include/

CXX ?= g++
CXXFLAGS ?= -O3 -Wall -ansi -pedantic -std=c++2a -I$(INCLUDEDIR) -I$(CPR_INCLUDEDIR) -c -fPIC -g
LDFLAGS ?= -shared -lssl -lpthread -lcurl -lcrypto

LIBRARY := awoken_discord
SOURCES := $(shell find $(SOURCEDIR)/ -type f -name "*.c*")
OBJECTS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.o, $(SOURCES))
DEPENDS := $(patsubst $(SOURCEDIR)/%, $(OBJECTDIR)/%.d, $(SOURCES))

BINARIES ?= /usr/lib
HEADERS ?= /usr/include

SHARED := lib$(LIBRARY).so
STATIC := lib$(LIBRARY).a

# Options, use make KEY=VALUE
USE_CPR ?= 1

USE_OPUS ?= 1
USE_SODIUM ?= 1
USE_VOICE ?= 1

USE_ASIO ?= 1
USE_WEBSOCKETPP ?= 1
USE_UWEBSOCKETS ?= 0
USE_CUSTOM_UDP_CLIENT ?= 0
# Change if using your own one

ifeq ($(USE_CPR), 1)
	CXXFLAGS += -DUSE_CPR=1
	LDFLAGS += -L$(CPR_BINARYDIR) -lcpr
else
	CXXFLAGS += -DUSE_CPR=0
endif
ifeq ($(USE_OPUS), 1)
	CXXFLAGS += -DUSE_OPUS=1
	LDFLAGS += -lopus
else
	CXXFLAGS += -DUSE_OPUS=0
endif
ifeq ($(USE_SODIUM), 1)
	CXXFLAGS += -DUSE_SODIUM=1
	LDFLAGS += -lsodium
else
	CXXFLAGS += -DUSE_SODIUM=0
endif
ifeq ($(USE_VOICE), 1)
	CXXFLAGS += -DUSE_VOICE=1
else
	CXXFLAGS += -DUSE_VOICE=0
endif

ifeq ($(USE_ASIO), 1)
	CXXFLAGS += -DUSE_ASIO=1
else
	CXXFLAGS += -DUSE_ASIO=0
endif
ifeq ($(USE_WEBSOCKETPP), 1)
	CXXFLAGS += -DUSE_WEBSOCKETPP=1
else
	CXXFLAGS += -DUSE_WEBSOCKETPP=0
endif
ifeq ($(USE_UWEBSOCKETS), 1)
	CXXFLAGS += -DUSE_UWEBSOCKETS=1
else
	CXXFLAGS += -DUSE_UWEBSOCKETS=0
endif
ifeq ($(USE_CUSTOM_UDP_CLIENT), 1)
	CXXFLAGS += -DUSE_CUSTOM_UDP_CLIENT=1
else
	CXXFLAGS += -DUSE_CUSTOM_UDP_CLIENT=0
endif

VERSION_BUILD := `git rev-list --count HEAD`
VERSION_BRANCH := `git rev-parse --abbrev-ref HEAD`
VERSION_HASH := `git rev-parse HEAD`
VERSION_IS_MASTER = 0
VERSION_DESCRIPTION_CONCAT := /
VERSION_DESCRIPTION := `git describe --always --long --dirty`

ifeq ($(VERSION_BRANCH), "master")
	VERSION_IS_MASTER = 1
endif

ifeq ($(VERSION_DESCRIPTION), "")
	VERSION_DESCRIPTION_CONCAT = /
endif

all: version $(LIBRARY)

version:
	@cp -f include/version.h.in include/version.h.out
	@sed -i include/version.h.out \
	-e "s/{AWOKEN_DISCORD_VERSION_BUILD}"/$(VERSION_BUILD)/g \
	-e "s/{AWOKEN_DISCORD_VERSION_BRANCH}"/$(VERSION_BRANCH)/g \
	-e "s/{AWOKEN_DISCORD_VERSION_HASH}"/$(VERSION_HASH)/g \
	-e "s/{AWOKEN_DISCORD_VERSION_IS_MASTER}"/$(VERSION_IS_MASTER)/g \
	-e "s/{AWOKEN_DISCORD_VERSION_DESCRIPTION_CONCAT}$(VERSION_DESCRIPTION_CONCAT)/g" \
	-e "s/{AWOKEN_DISCORD_VERSION_DESCRIPTION}"/$(VERSION_DESCRIPTION)/g
	@cmp -s include/version.h include/version.h.out; if [ $$? -ne 0 ]; then \
		cp include/version.h.out include/version.h; \
		echo "Created version.h file."; \
	fi;

install:
	cp -f $(BUILDDIR)/$(SHARED) $(BINARIES)/
	cp -f $(BUILDDIR)/$(STATIC) $(BINARIES)/
	mkdir -p $(HEADERS)/$(LIBRARY)
	cp -rf $(INCLUDEDIR)/* $(HEADERS)/$(LIBRARY)
	cp -rf $(CPR_INCLUDEDIR)cpr $(HEADERS)/
	rm $(HEADERS)/$(LIBRARY)/version.h.in

uninstall:
	rm -f $(BINARIES)/$(SHARED)
	rm -f $(BINARIES)/$(STATIC)
	rm -rf $(HEADERS)/$(LIBRARY)
	rm -rf $(HEADERS)/cpr

$(OBJECTDIR)/%.o: $(SOURCEDIR)/%
	@echo "]> \033[36mBuilding "$@"...\033[0m"
	@mkdir -p `dirname $@`
	$(CXX) $(CXXFLAGS) -MMD -MP $< -o $@

-include $(DEPENDS)

$(LIBRARY): $(OBJECTS)
	@echo "]> \033[32mBuilding shared and static libraries...\033[0m"
	@mkdir -p $(BUILDDIR)
	@$(CXX) -o $(BUILDDIR)/$(SHARED) $^ $(LDFLAGS)
	@ar rcs $(BUILDDIR)/$(STATIC) $^

clean:
	rm -rf $(OBJECTDIR)
	rm -rf $(BUILDDIR)
