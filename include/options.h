#pragma once
// Set options here or override with makefile in the format DEFINED=VALUE
// Example: `make USE_ASIO=0`

#ifndef USE_CPR
	#define USE_CPR 1
#endif

#ifndef USE_ASIO
	#define USE_ASIO 1
#endif
#ifndef USE_WEBSOCKETPP
	#define USE_WEBSOCKETPP 1
#endif
#ifndef USE_CUSTOM_UDP_CLIENT
	#define USE_CUSTOM_UDP_CLIENT 0
#endif

#ifndef USE_OPUS
	#define USE_OPUS 1
#endif
#ifndef USE_SODIUM
	#define USE_SODIUM 1
#endif
#ifndef USE_VOICE
	#define USE_VOICE 1
#endif
