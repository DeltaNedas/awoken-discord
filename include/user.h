#pragma once
#include <string>
#include "discord_object_interface.h"
#include "snowflake.h"
#include "permissions.h"

namespace AwokenDiscord {
	// https://discordapp.com/developers/docs/resources/user#user-object
	struct User : public IdentifiableDiscordObject<User> {
	public:
		User() {}
		~User() {}

		std::string username;
		std::string discriminator;
		std::string avatar;
		bool bot = false;
		bool mfaEnabled = false;
		bool verified = false; // "email" scope required
		std::string email = ""; // ^
		int flags = 0;
		int premiumType = 0; // Nitro type, 0: None 1: Nitro Classic 2: Nitro
	};

	void to_json(json& to, const User& from);
	void from_json(const json& from, User& to);

	// https://discordapp.com/developers/docs/resources/user#connection-object
	struct Connection : public IdentifiableDiscordObject<Connection> {
	public:
		Connection() {}
		~Connection() {}

		std::string name;
		std::string type;
		bool revoked;
	};

	void to_json(json& to, const Connection& from);
	void from_json(const json& from, Connection& to);
}