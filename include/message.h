#pragma once

#include "attachment.h"
#include "channel.h"
#include "discord_object_interface.h"
#include "embed.h"
#include "emoji.h"
#include "permissions.h"
#include "snowflake.h"
#include "user.h"
#include "webhook.h"

namespace AwokenDiscord {
	class BaseDiscordClient;
	struct Server;

	struct Application;
	// https://discordapp.com/developers/docs/resources/channel#message-object-message-application-structure
	struct MessageApplication : public IdentifiableDiscordObject<Application> {
	public:
		inline bool empty() const {
			return ID.empty();
		}

		std::string cover = "";
		std::string description = "";
		std::string icon =  "";
		std::string name = "";
	};

	void to_json(json& to, const MessageApplication& from);
	void from_json(const json& from, MessageApplication& to);

	// https://discordapp.com/developers/docs/resources/channel#message-object-message-reference-structure
	struct MessageOrigin : public IdentifiableDiscordObject<Message> {
	public:
		inline bool empty() const {
			return ID.empty() || channelID.empty() || serverID.empty();
		}

		Snowflake<Channel> channelID;
		Snowflake<Server> serverID;
	};

	void to_json(json& to, const MessageOrigin& from);
	void from_json(const json& from, MessageOrigin& to);


	// https://discordapp.com/developers/docs/resources/channel#message-object
	struct Message : public IdentifiableDiscordObject<Message> {
	public:
		// Helper functions
		bool wasPinged(Snowflake<User> ID);
		bool wasPinged(User& user);
		Message send(BaseDiscordClient* client);
		Message reply(BaseDiscordClient* client, std::string content, Embed embed = Embed(), bool tts = false);

		Snowflake<Channel> channelID;
		Snowflake<Server> serverID;
		User author; // Only a valid user if webhookID is invalid.
		ServerMember member; // Only sent if message is in a text server channel.
		std::string content = "";
		std::string timestamp = "";
		std::string editedAt = ""; // ISO 8601
		bool tts = false;
		bool pingedEveryone = false;
		std::vector<User> pingedUsers;
		std::vector<Snowflake<Role>> pingedRoles;
		std::vector<Channel> pingedChannels; // Incomplete channel, conforms to https://discordapp.com/developers/docs/resources/channel#channel-mention-object
		std::vector<Attachment> attachments;
		std::vector<Embed> embeds;
		std::vector<Reaction> reactions;
		std::string nonce = "";
		bool pinned = false;
		Snowflake<Webhook> webhookID;

		// https://discordapp.com/developers/docs/resources/channel#message-object-message-types
		enum MessageType {
			DEFAULT = 0,
			RECIPIENT_ADD = 1,
			RECIPIENT_REMOVE = 2,
			CALL = 3,
			CHANNEL_NAME_CHANGE = 4,
			CHANNEL_ICON_CHANGE = 5,
			CHANNEL_PINNED_MESSAGE = 6,
			GUILD_MEMBER_JOIN = 7,
			BOOST = 8,
			BOOST_TIER_1 = 9,
			BOOST_TIER_2 = 10,
			BOOST_TIER_3 = 11,
			CHANNEL_FOLLOW_ADD = 12 // When you click follow button in a news channel
		} type = DEFAULT;


		// https://discordapp.com/developers/docs/resources/channel#message-object-message-activity-types
		enum class MessageActivityType {
			NONE = 0,
			JOIN = 1,
			SPECTATE = 2,
			LISTEN = 3,
			JOIN_REQUEST = 5
		} activity; // For fancy integrations like spotify
		MessageApplication application; // ^
		MessageOrigin origin; // For crossposts

		// https://discordapp.com/developers/docs/resources/channel#message-object-message-flags
		enum class MessageFlags {
			NONE = 0,
			CROSSPOSTED = 1 << 0,
			IS_CROSSPOST = 1 << 1,
			SUPPRESS_EMBEDS = 1 << 2,
			SOURCE_MESSAGE_DELETED = 1 << 3,
			URGENT = 1 << 4 // mr drampf ebil botnet
		}  flags;
	};

	void to_json(json& to, const Message& from);
	void from_json(const json& from, Message& to);


	struct MessageRevisions {
		MessageRevisions(const json& data) :
			messageID(data["id"]), channelID(data["channel_id"]), revisions(data) {}

		inline void applyChanges(Message& outOfDateMessage) {
			assert(outOfDateMessage.ID == messageID);
			outOfDateMessage = revisions;
		}
		Snowflake<Message> messageID;
		Snowflake<Channel> channelID;
		const json& revisions;
	};

	// https://discordapp.com/developers/docs/resources/channel#create-message-params
	// Part of this, the rest is created by client->sendMesage.
	struct CreateMessageParams : public DiscordObject {
	public:
		inline bool empty() const {
			return content.empty() && embed.empty();
		}

		Snowflake<Channel> channelID;
		std::string content = "";
		Embed embed = Embed::Flag::INVALID_EMBED;
		bool tts = false;
	};

	void to_json(json& to, const CreateMessageParams& from);
	void from_json(const json& from, CreateMessageParams& to);
}
