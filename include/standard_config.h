#pragma once
#ifndef AWOKEN_DO_NOT_INCLUDE_STANDARD_ONERROR
#include "debug_level.h"
void DiscordClient::onError(AwokenDiscord::ErrorCode errorCode, const std::string errorMessage) {
	if (debugLevel & DebugLevel::ERROR) {
		if (errorCode != 0) {
			std::cout << "\033[31mAwoken Discord encountered an error: " << errorCode << " (" + errorMessage + ")\033[0m\n";
		} else {
			std::cout << "\033[31mAwoken Discord encountered an error: " + errorMessage + "\033[0m\n";
		}
	}
}
#endif

#ifndef AWOKEN_DO_NOT_INCLUDE_STANDARD_SLEEP
void DiscordClient::sleep(const unsigned int milliseconds) {
	std::this_thread::sleep_for(std::chrono::milliseconds(milliseconds));
}
#endif