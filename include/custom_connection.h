#pragma once
#include <memory>
#include "websocket.h"

namespace AwokenDiscord {
	typedef std::shared_ptr<GenericWebsocketConnection> WebsocketConnection;
}