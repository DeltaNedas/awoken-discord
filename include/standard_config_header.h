#pragma once
#ifndef AWOKEN_DO_NOT_INCLUDE_STANDARD_ONERROR
virtual void onError(AwokenDiscord::ErrorCode errorCode, const std::string errorMessage) override;
#endif

#ifndef AWOKEN_DO_NOT_INCLUDE_STANDARD_SLEEP
virtual void sleep(const unsigned int milliseconds) override;
#endif