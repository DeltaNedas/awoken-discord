#pragma once
#include <string>

#include "discord_object_interface.h"
#include "snowflake.h"

namespace AwokenDiscord {
	// https://discordapp.com/developers/docs/resources/channel#attachment-object
	struct Attachment : public IdentifiableDiscordObject<Attachment> {
	public:
		std::string filename = "";
		size_t size = 0;
		std::string url = "";
		std::string proxyUrl = "";
		size_t height = 0;
		size_t width = 0;
	};

	void to_json(json& to, const Attachment& from);
	void from_json(const json& from, Attachment& to);
}
