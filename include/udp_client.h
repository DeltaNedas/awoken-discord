#pragma once
#include "options.h"
#include "udp.h"

#if USE_CUSTOM_UDP_CLIENT
	#include "custom_udp_client.h"
#elif defined(AWOKEN_UDP_CLIENT)
	typedef AWOKEN_UDP_CLIENT UDPClient
#else
	#if USE_ASIO
		#include "asio_udp.h"
	#else
		#include "custom_udp_client.h" // Fallback
	#endif
#endif
