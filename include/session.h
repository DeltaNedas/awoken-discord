#pragma once
#include "http.h"
#include "options.h"

//custom dynamic
#if AWOKEN_CUSTOM_SESSION
	#include "custom_session.h"

	//defined
	#elif defined(AWOKEN_SESSION) || defined(AWOKEN_SESSION_INCLUDE)
		#if AWOKEN_SESSION_INCLUDE
			#include AWOKEN_SESSION_INCLUDE
		#endif
		#if AWOKEN_SESSION
			typedef AWOKEN_SESSION Session
		#endif

	//defaults
#else
	#if USE_CPR
		#include "cpr_session.h"
	#else
		#include "custom_session.h"
	#endif
#endif