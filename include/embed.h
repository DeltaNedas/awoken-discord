#pragma once
#include <string>
#include "discord_object_interface.h"

namespace AwokenDiscord {
	struct CreateMessageParams;

	// https://discordapp.com/developers/docs/resources/channel#embed-object-embed-thumbnail-structure
	struct EmbedThumbnail : public DiscordObject {
	public:
		EmbedThumbnail() {}
		~EmbedThumbnail() {}

		std::string url;
		std::string proxyUrl;
		long height = 0;
		long width = 0;

		bool empty() const {
			return url.empty();
		}
	};

	void to_json(json& to, const EmbedThumbnail& from);
	void from_json(const json& from, EmbedThumbnail& to);


	// https://discordapp.com/developers/docs/resources/channel#embed-object-embed-video-structure
	struct EmbedVideo : public DiscordObject {
	public:
		EmbedVideo() {}
		~EmbedVideo() {}

		std::string url;
		long height = 0;
		long width = 0;

		bool empty() const {
			return url.empty();
		}
	};

	void to_json(json& to, const EmbedVideo& from);
	void from_json(const json& from, EmbedVideo& to);


	// https://discordapp.com/developers/docs/resources/channel#embed-object-embed-image-structure
	struct EmbedImage : public DiscordObject {
	public:
		EmbedImage() {}
		~EmbedImage() {}

		std::string url;
		std::string proxyUrl;
		long height = 0;
		long width = 0;

		bool empty() const {
			return url.empty() && proxyUrl.empty();
		}
	};

	void to_json(json& to, const EmbedImage& from);
	void from_json(const json& from, EmbedImage& to);


	// https://discordapp.com/developers/docs/resources/channel#embed-object-embed-provider-structure
	struct EmbedProvider : public DiscordObject {
	public:
		EmbedProvider() {}
		~EmbedProvider() {}

		std::string name;
		std::string url;

		bool empty() const {
			return name.empty() && url.empty();
		}
	};

	void to_json(json& to, const EmbedProvider& from);
	void from_json(const json& from, EmbedProvider& to);


	// https://discordapp.com/developers/docs/resources/channel#embed-object-embed-author-structure
	struct EmbedAuthor : public DiscordObject {
	public:
		EmbedAuthor() {}
		~EmbedAuthor() {}
		std::string name;
		std::string url;
		std::string iconUrl;
		std::string proxyIconUrl;

		bool empty() const {
			return name.empty() && url.empty() && iconUrl.empty() && proxyIconUrl.empty();
		}
	};

	void to_json(json& to, const EmbedAuthor& from);
	void from_json(const json& from, EmbedAuthor& to);


	// https://discordapp.com/developers/docs/resources/channel#embed-object-embed-footer-structure
	struct EmbedFooter : public DiscordObject {
	public:
		EmbedFooter() {}
		~EmbedFooter() {}

		std::string text;
		std::string iconUrl;
		std::string proxyIconUrl;

		bool empty() const {
			return text.empty();
		}
	};

	void to_json(json& to, const EmbedFooter& from);
	void from_json(const json& from, EmbedFooter& to);


	// https://discordapp.com/developers/docs/resources/channel#embed-object-embed-field-structure
	struct EmbedField : public DiscordObject {
	public:
		EmbedField() {}
		~EmbedField() {}

		std::string name = "";
		std::string value = "";
		bool isInline = false;

		bool empty() const {
			return value.empty();
		}
	};

	void to_json(json& to, const EmbedField& from);
	void from_json(const json& from, EmbedField& to);


	// https://discordapp.com/developers/docs/resources/channel#embed-object
	struct Embed : public DiscordObject {
	public:
		Embed() {}
		~Embed() {}

		std::string title;
		std::string type;
		std::string description;
		std::string url;
		std::string timestamp;
		int32_t color = -1;
		EmbedFooter footer;
		EmbedImage image;
		EmbedThumbnail thumbnail;
		EmbedVideo video;
		EmbedProvider provider;
		EmbedAuthor author;
		std::vector<EmbedField> fields;

		bool empty() const {
			return (flags == Flag::INVALID_EMBED) || (title.empty() && description.empty() && url.empty() &&
				color == -1 && footer.empty() && image.empty() && thumbnail.empty() && video.empty() &&
				provider.empty() && author.empty() && fields.empty());
		}

		enum class Flag {
			INVALID_EMBED = 0,
			VALID_EMBED = 1
		};
		Flag flags = Flag::VALID_EMBED;
		Embed(const Flag f) : flags(f) {}
	private:
		friend BaseDiscordClient;
		friend CreateMessageParams;
	};

	void to_json(json& to, const Embed& from);
	void from_json(const json& from, Embed& to);
}
