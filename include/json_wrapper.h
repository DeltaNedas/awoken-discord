#pragma once
#include <iostream>
#include <map>

#include "json.hpp"

namespace AwokenDiscord {
	using nlohmann::json;
	extern const json null;

	enum FieldType {
		REQUIRED = 1,
		OPTIONAL = 2,
		NULLABLE = 4,
		OPTIONAL_NULLABLE = 6
	};

	template <class T>
	inline T toEnum(const json& value) {
		return static_cast<T>(value.get<long>());
	}

	template <class T>
	inline void toEnum(const json& value, T& to) {
		to = toEnum<T>(value);
	}

	template <class T>
	inline long fromEnum(const T& value) {
		return static_cast<long>(value);
	}

	template <class T>
	inline void fromEnum(const T& value, long& to) {
		to = fromEnum(value);
	}

	template <class T>
	inline bool isEmpty(const T& value, const T& empty = T()) {
		return value.empty();
	}
	inline bool isEmpty(size_t value, const size_t empty = 0) {
		return value == empty;
	}
	inline bool isEmpty(long value, const long empty = 0) {
		return value == empty;
	}
	inline bool isEmpty(unsigned value, const unsigned empty = 0) {
		return value == empty;
	}
	inline bool isEmpty(int value, const int empty = 0) {
		return value == empty;
	}
	inline bool isEmpty(short value, const short empty = 0) {
		return value == empty;
	}
	inline bool isEmpty(char value, const char empty = 0) {
		return value == empty;
	}
	inline bool isEmpty(double value, const double empty = 0) {
		return value == empty;
	}
	inline bool isEmpty(float value, const float empty = 0) {
		return value == empty;
	}
	inline bool isEmpty(bool value, const bool empty = false) {
		return value == empty;
	}

	template <class T>
	void addJsonField(json& j, const std::string& key, const T& value, FieldType type = OPTIONAL, const T& empty = T()) {
		if (isEmpty(value, empty) && !(type & REQUIRED)) {
			if (type & NULLABLE) {
				//j[key] = null;
			}
		} else {
			j[key] = value;
		}
	}

	template <class T>
	void _setJsonField(const json& j, T& value) {
		j.get_to(value);
	}
	inline void _setJsonField(const json& j, json& value) {
		value = j;
	}
	template <class T>
	void getJsonField(const json& j, const std::string& key, T& value, FieldType type = OPTIONAL) {
		if (j.is_object()) {
			auto found = j.find(key);
			if (found == j.end()) {
				if (type & REQUIRED) {
					throw std::runtime_error("JSON " + j.dump(1, '\t') + " does not contain required field " + key + "!");
				}
			} else {
				if (found.value().is_null()) {
					if (!(type & NULLABLE)) {
						throw std::runtime_error("JSON " + j.dump(1, '\t') + " has null field " + key + "!");
					}
				} else {
					_setJsonField(found.value(), value);
				}
			}
		}
	}
}
