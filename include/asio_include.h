#pragma once
#define ASIO_STANDALONE

#ifdef _WIN32
	#include <winsock2.h>
#elif defined(unix) || defined(__unix__) || defined(__unix)
	#include <netinet/in.h>
#endif

#include <asio.hpp>
