#pragma once
#include "discord_object_interface.h"
#include "snowflake.h"
#include "channel.h"

namespace AwokenDiscord {
	struct Server;
	struct Channel;
	struct User;

	// https://discordapp.com/developers/docs/resources/voice#voice-state-object
	struct VoiceState : public DiscordObject {
		VoiceState() {}
		~VoiceState() {}

		Snowflake<Server> serverID;
		Snowflake<Channel> channelID;
		Snowflake<User> userID;
		std::string sessionID;
		bool deaf = false;
		bool mute = false;
		bool selfDeaf = false;
		bool selfMute = false;
		bool suppress = false;
	};

	void to_json(json& to, const VoiceState& from);
	void from_json(const json& from, VoiceState& to);


	// https://discordapp.com/developers/docs/resources/voice#voice-region-object
	struct VoiceRegion : IdentifiableDiscordObject<VoiceRegion> {
		VoiceRegion() {}
		~VoiceRegion() {}

		std::string name;
		bool vip = false;
		bool optimal = false;
		bool deprecated = false;
		bool custom = false;
	};

	void to_json(json& to, const VoiceState& from);
	void from_json(const json& from, VoiceState& to);


	// https://discordapp.com/developers/docs/topics/gateway#voice-server-update-voice-server-update-event-fields
	struct VoiceServerUpdate : DiscordObject {
		VoiceServerUpdate() {}
		~VoiceServerUpdate() {}

		std::string token;
		Snowflake<Server> serverID;
		std::string endpoint;
	};

	void to_json(json& to, const VoiceServerUpdate& from);
	void from_json(const json& from, VoiceServerUpdate& to);
}