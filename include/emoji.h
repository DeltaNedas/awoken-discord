#pragma once

#include "discord_object_interface.h"
#include "permissions.h"
#include "user.h"

namespace AwokenDiscord {
	// https://discordapp.com/developers/docs/resources/emoji#emoji-object-emoji-structure
	struct Emoji : public IdentifiableDiscordObject<Emoji> {
	public:
		Emoji() {}
		~Emoji() {}

		std::string name; // Only empty in reaction.emoji
		std::vector<Role> roles;
		User user;
		bool requireColons = false;
		bool managed = false;
		bool animated = false;
	};

	void to_json(json& to, const Emoji& from);
	void from_json(const json& from, Emoji& to);


	// https://discordapp.com/developers/docs/resources/channel#reaction-object
	struct Reaction : public DiscordObject {
	public:
		Reaction() {}
		~Reaction() {}

		int count = 0;
		bool me = false;
		Emoji emoji;
	};

	void to_json(json& to, const Reaction& from);
	void from_json(const json& from, Reaction& to);
}