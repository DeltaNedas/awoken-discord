#pragma once
#include "error.h"
#include "http.h"
#include "json_wrapper.h"
#include "snowflake.h"

#include <list>

namespace AwokenDiscord {
	class BaseDiscordClient;

	class DiscordObject {
#define modIfElse(condition, modifier, value, el) \
			condition(value) ? modifier(value) : el

#define setIfElse(condition, value, el) \
			condition(value) ? value : el
	};

	template <class Derived>
	class IdentifiableDiscordObject : public DiscordObject {
	public:
		IdentifiableDiscordObject() = default;
		IdentifiableDiscordObject(Snowflake<Derived> id) : ID(id) {}

		using Parent = IdentifiableDiscordObject<Derived>;

		Snowflake<Derived> ID;

		inline operator Snowflake<Derived>&() {
			return ID;
		}

		inline bool empty() const {
			return ID.empty();
		}

		template <class DiscordObject>
		inline bool operator==(const Snowflake<DiscordObject>& right) const {
			return ID == static_cast<Snowflake<DiscordObject>>(right);
		}

		inline bool operator==(const Snowflake<Derived>& right) const {
			return operator==<Derived>(right);
		}

		inline bool operator==(const IdentifiableDiscordObject<Derived>& right) const {
			return ID == right.ID;
		}

		inline const size_t getsize_tstamp() {
			return ID.timestamp();
		}
	};

	//constexpr unsigned int index(std::initializer_list<const char *const> names, const char * name, unsigned int i = 0) {
	//	for (const char *const n : names)
	//		if (strcmp(n, name) != 0) ++i;
	//		else break;
	//	return i;
	//}//sadly this doesn't work on c++11, leaving this here for the future

	template <template <class...> class Container, typename Type> //forces this be done at compile time, I think, and hope it does
	constexpr unsigned int index(Container<Type*const> names, Type * name, unsigned int i = 0) {
		return i + names.begin() != names.end() && strcmp(*(i + names.begin()), name) != 0 ? index(names, name, i + 1) : i;
	}
}