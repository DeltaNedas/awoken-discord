#pragma once
#include "websocketpp_common.h"

namespace AwokenDiscord {
#if USE_WEBSOCKETPP
	typedef websocketpp::connection_hdl WebsocketConnection;
#endif
}