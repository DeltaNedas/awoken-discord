#pragma once
#include "version.h"
#include <unordered_set>
namespace AwokenDiscord {

	//thanks https://stackoverflow.com/a/5459929
	//convert preprocessor number into a string
	//for example:
	//#define AWOKEN_DISCORD_VERSION_BUILD 540
	//AWOKEN_DISCORD_VERSION_STR(BUILD) gives us "540"
#define AWOKEN_STR_HELPER(x) #x
#define AWOKEN_STR_HELPER2(x) AWOKEN_STR_HELPER(x)
#define AWOKEN_STR_HELPER3(x, y) x##y
#define AWOKEN_DISCORD_VERSION_STR(x) \
	AWOKEN_STR_HELPER2(AWOKEN_STR_HELPER3(AWOKEN_DISCORD_VERSION_, x))

	//please only use defines when you want to check version via preprocessors
	//uses xxxxyyyyyy format, which can be converted to xxxx.yyyyyy
#define AWOKEN_DISCORD_VERSION_NUM 0

#if defined NONEXISTANT_VERSION_H || defined NONEXISTANT_GIT_INFO
	#define AWOKEN_DISCORD_VERSION_BUILD 0
	#define AWOKEN_DISCORD_VERSION_BRANCH "unknown branch"
	#define AWOKEN_DISCORD_VERSION_HASH "unknown revision"
	#define AWOKEN_DISCORD_VERSION_IS_MASTER 0
	//letter to use for concat description
	#define AWOKEN_DISCORD_VERSION_DESCRIPTION_CONCAT " "
	#define AWOKEN_DISCORD_VERSION_DESCRIPTION "unknown"
#endif

#define AWOKEN_DISCORD_VERSION \
	AWOKEN_DISCORD_VERSION_STR(NUM) "-"\
	AWOKEN_DISCORD_VERSION_STR(BUILD) " "\
	AWOKEN_DISCORD_VERSION_BRANCH \
	AWOKEN_DISCORD_VERSION_DESCRIPTION_CONCAT \
	AWOKEN_DISCORD_VERSION_DESCRIPTION

	constexpr unsigned int versionNum = AWOKEN_DISCORD_VERSION_NUM;
	constexpr unsigned int revisionNum = AWOKEN_DISCORD_VERSION_BUILD;
	//for some reason const fixes a warning about convering a char* to a const char*
	constexpr const char* description = AWOKEN_DISCORD_VERSION_DESCRIPTION;
	constexpr const char* branch = AWOKEN_DISCORD_VERSION_BRANCH;
	constexpr const char* revision = AWOKEN_DISCORD_VERSION_HASH;
	constexpr const char* version = AWOKEN_DISCORD_VERSION;
	constexpr bool isMaster = AWOKEN_DISCORD_VERSION_IS_MASTER;
	constexpr const char* userAgent =
		"DiscordBot (https://bitbucket.org/DeltaNedas/AwokenDiscord, " \
		AWOKEN_DISCORD_VERSION_STR(NUM) \
		") " \
		AWOKEN_DISCORD_VERSION \
	;

	//Features
	//Remember to list features in both preprocessers and unordered_set
#define AWOKEN_FEATURE_AVAILABLE_FEATURE_LIST
#define AWOKEN_FEATURE_LIST_OF_AVAILABLE_FEATURES //fixed grammer
	std::unordered_set<std::string> availableFeatures{
		"Available Feature List"
		"List of Available Features"
	};
	inline bool isFeatureAvaiable(std::string& featureName) {
		return availableFeatures.find(featureName) != availableFeatures.end();
	}
}
