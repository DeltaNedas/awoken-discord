#pragma once
#include <string>

#include "permissions.h"
#include "snowflake.h"
#include "user.h"

namespace AwokenDiscord {
	// https://discordapp.com/developers/docs/resources/channel#overwrite-object
	struct Overwrite : IdentifiableDiscordObject<Overwrite> {
	public:
		Overwrite(std::string type = "", Permission allow = Permission::NONE, Permission deny = Permission::NONE);
		~Overwrite() {};

		std::string type = "";
		Permission allow = Permission::NONE;
		Permission deny = Permission::NONE;
	};

	void to_json(json& to, const Overwrite& from);
	void from_json(const json& from, Overwrite& to);


	struct Server;
	struct Message;

	// https://discordapp.com/developers/docs/resources/channel#channel-object
	struct Channel : IdentifiableDiscordObject<Channel> {
	public:
		Channel() {}
		~Channel() {}

		// https://discordapp.com/developers/docs/resources/channel#channel-object-channel-types
		enum ChannelType {
			CHANNEL_TYPE_NONE = -1,
			SERVER_TEXT = 0,
			DM = 1,
			SERVER_VOICE = 2,
			GROUP_DM = 3,
			SERVER_CATEGORY = 4,
			GUILD_NEWS = 5,
			GUILD_STORE = 6
		} type = CHANNEL_TYPE_NONE;
		Snowflake<Server> serverID;
		int position = 0;
		std::vector<Overwrite> permissionOverwrites;
		std::string name;
		std::string topic;
		bool nsfw = false;
		Snowflake<Message> lastMessageID;
		int bitrate = 0;
		int userLimit = 0;
		std::vector<User> recipients;
		std::string icon;
		Snowflake<User> ownerID;
		Snowflake<User> applicationID;
		Snowflake<Channel> parentID;
		std::string lastPinTimestamp;
	};

	void to_json(json& to, const Channel& from);
	void from_json(const json& from, Channel& to);
}