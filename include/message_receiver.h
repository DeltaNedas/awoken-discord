#pragma once
#include <string>
#include "websocket_connection.h"

namespace AwokenDiscord {
	class GenericMessageReceiver {
	public:
		virtual ~GenericMessageReceiver() = default;
		virtual void initialize() {} // Called when ready to recevie messages
		virtual void handleFailToConnect() {} // Called when connection has failed to start
		virtual void processMessage(const std::string& message) = 0; // Called when recevicing a message
		virtual void processCloseCode(const int16_t /*code*/) {}
		WebsocketConnection connection; // TODO: Maybe this should be set to protected?
	};
}