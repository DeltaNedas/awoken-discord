#pragma once

#include "channel.h"
#include "discord_object_interface.h"
#include "server.h"
#include "snowflake.h"
#include "user.h"

namespace AwokenDiscord {
	// https://discordapp.com/developers/docs/resources/webhook#webhook-object
	struct Webhook : public IdentifiableDiscordObject<Webhook> {
	public:
		Webhook() {}
		~Webhook() {}

		int type; // 1: Normal webhook 2: Crossposted messages from updates channels
		Snowflake<Server> serverID;
		Snowflake<Channel> channelID;
		User user;
		std::string name;
		std::string avatar;
		std::string token;
	};

	void to_json(json& to, const Webhook& from);
	void from_json(const json& from, Webhook& to);
}