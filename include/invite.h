#pragma once
#include <string>

#include "channel.h"
#include "discord_object_interface.h"
#include "server.h"
#include "snowflake.h"
#include "user.h"

namespace AwokenDiscord {
	// https://discordapp.com/developers/docs/resources/invite#invite-object
	struct Invite : public DiscordObject {
	public:
		Invite() {}
		~Invite() {}

		std::string code;
		Server server;
		Channel channel;
		User user;
		int userType;
		size_t approxPresenceCount;
		size_t approxMemberCount;
	};

	void to_json(json& to, const Invite& from);
	void from_json(const json& from, Invite& to);


	// https://discordapp.com/developers/docs/resources/invite#invite-metadata-object
	struct InviteMetadata : public DiscordObject {
		InviteMetadata() {}
		~InviteMetadata() {}

		User inviter;
		int uses = 0;
		int maxUses = 0;
		int maxAge = 0;
		bool isTemporary;
		std::string createdAt;
	};

	void to_json(json& to, const InviteMetadata& from);
	void from_json(const json& from, InviteMetadata& to);
}