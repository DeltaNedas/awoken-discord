#pragma once

namespace AwokenDiscord {
	namespace DebugLevel {
		const unsigned char NONE = 0;
		const unsigned char START = 1 << 0;
		const unsigned char CLOSE = 1 << 1;
		const unsigned char SOCKET = 1 << 2;
		const unsigned char ERROR = 1 << 3;
		const unsigned char SEND_MESSAGE = 1 << 4;
		const unsigned char DEBUG = 1 << 5;
		const unsigned char STATUS = 1 << 6;
	}
}