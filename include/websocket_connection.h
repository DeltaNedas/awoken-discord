#pragma once

//custom dynamic
#if USE_CUSTOM_WEBSOCKETS_CONNECTION
	#include "custom_connection.h"

//defined
#elif defined(AWOKEN_WEBSOCKETS_CONNECTION_INCLUDE) || defined(AWOKEN_WEBSOCKETS_CONNECTION)
	#ifdef AWOKEN_WEBSOCKETS_CONNECTION_INCLUDE
		#include AWOKEN_WEBSOCKETS_CONNECTION_INCLUDE
	#endif
	#ifdef AWOKEN_WEBSOCKETS_CONNECTION
		typedef AWOKEN_SESSION Session
	#endif
//defaults
#else
	#ifndef AWOKEN_LOCK_EXISTENT_TO
		#include "websocketpp_connection.h"
		#if !USE_WEBSOCKETPP
			// last resort
			#include "custom_connection.h"
		#endif
	#elif AWOKEN_LOCK_EXISTENT_TO == AWOKEN_WEBSOCKETPP
		#include "websocketpp_connection.h"
	#else
		// last resort
		#include "custom_connection.h"
	#endif
#endif
