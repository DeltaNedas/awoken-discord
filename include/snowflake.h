#pragma once
#include <string>
#include <chrono>
#include <algorithm>
#if defined(__cpp_exceptions) || defined(__EXCEPTIONS)
#	include <stdexcept>
#endif

#include "json_wrapper.h"

namespace AwokenDiscord {
	// Stops you from mixing up different types of ids, like using a message_id as a user_id
	template <typename DiscordObject>
	class Snowflake {
	public:
		Snowflake() {}
		template <typename T>
		Snowflake(const Snowflake<T>& old) {
			raw = old;
		}
		~Snowflake() {}

		Snowflake(const std::string& snow) : raw(snow) {}
		Snowflake(const char* snow) : raw(std::string(snow)) {}
		Snowflake(const Snowflake& flake) : Snowflake(flake.string()) {}
		Snowflake(const DiscordObject& object) : Snowflake(object.ID) {}
		Snowflake(const long number) : Snowflake(std::to_string(number)) {}
		Snowflake(const json& value) : Snowflake(value.is_string() ? value.get<std::string>() : "") {}

		inline bool operator==(const Snowflake& right) const {
			return raw == right.raw;
		}
		inline bool operator!=(const Snowflake& right) const {
			return raw != right.raw;
		}

		inline bool operator==(std::string& right) const {
			return raw == right;
		}
		inline bool operator!=(std::string& right) const {
			return raw != right;
		}

		inline operator const std::string&() const { return raw; }
		inline const bool empty() const { return raw.empty(); }

		inline const std::string& string() const { return raw; }
		inline const long number() const { return std::stoll(raw); }

		// Get milliseconds since 2015 this object was created at.
		std::chrono::time_point<std::chrono::steady_clock> timestamp() const {
#if defined(__cpp_exceptions) || defined(__EXCEPTIONS)
			if (raw.empty()) throw std::invalid_argument("Invalid snow in Snowflake!");
#endif
			return std::chrono::time_point<std::chrono::steady_clock>(std::chrono::milliseconds((std::stoll(raw) >> 22) + discordEpoch));
		}

		template <class iterator>
		inline iterator findObject(iterator begin, iterator end) const {
			return std::find_if(begin, end, [&](const DiscordObject& object) {
				return operator==(static_cast<DiscordObject>(object));
			});
		}

		// Magical code from stack overflow
		// https://stackoverflow.com/a/87846
		template <class Container>
		struct HasAFindFunction {
			using SuccessType = char;
			using FailureType = int;
			template <class _Container, size_t (_Container::*)() const> struct Magic {};
			template <class _Container> static SuccessType Test(Magic<_Container, &_Container::find>*);
			template <class _Container> static FailureType Test(...);
			static const bool Value = sizeof(Test<Container>(0)) == sizeof(SuccessType);
		};

		template <class Container>
		auto findObject(Container& objects, std::true_type) const -> decltype(objects.begin()) {
			return objects.find(operator const std::string&());
		}

		template <class Container>
		auto findObject(Container& objects, std::false_type) const -> decltype(objects.begin()) {
			return findObject(objects.begin(), objects.end());
		}

		template <class Container>
		auto findObject(Container& objects) const -> decltype(objects.begin()) {
			return findObject(objects, std::integral_constant<bool, HasAFindFunction<Container>::Value>());
		}

	private:
		std::string raw = "";
		static const size_t discordEpoch = 1420070400000; // First millisecond of 2015 after UNIX epoch
	};

	template <class T>
	void to_json(json& to, const Snowflake<T>& from) {
		to = from.string();
	}

	template <class T>
	void from_json(const json& from, Snowflake<T>& to) {
		to = Snowflake<T>(from.get<std::string>());
	}
}