#pragma once
#include <array>
#include <time.h>

#include "discord_object_interface.h"
#include "user.h"
#include "channel.h"

namespace AwokenDiscord {
	// https://discordapp.com/developers/docs/resources/guild#unavailable-guild-object
	// Should be in servers.h but include loops >:(
	struct UnavailableServer : public IdentifiableDiscordObject<Server> {
		UnavailableServer() {}
		~UnavailableServer() {}

		//const bool unavailable = true;
	};

	void to_json(json& to, const UnavailableServer& from);
	void from_json(const json& from, UnavailableServer& to);

	enum Status {
		INVALID = -1,
		ONLINE = 0,
		DND = 1,
		IDLE = 2,
		INVISIBLE = 3,
		OFFLINE = 4
	};

	enum GameType {
		Playing = 0, //calling this Game causes issues
		Streaming = 1
	};

	struct Game : public DiscordObject {
		std::string name = "";
		GameType type;
		std::string url = ""; //used when type is Streaming
	};

	// https://discordapp.com/developers/docs/topics/gateway#ready
	struct Ready : public DiscordObject {
	public:
		Ready() {}
		~Ready() {}

		int v; // Gateway protocol version
		User user;
		std::list<Channel> privateChannels;
		std::list<UnavailableServer> servers;
		std::string sessionID;
		std::array<int, 2> shard = {{0, 1}};
	};

	void to_json(json& to, const Ready& from);
	void from_json(const json& from, Ready& to);


	// https://discordapp.com/developers/docs/topics/gateway#activity-object-activity-timestamps
	struct ActivityTimestamp : public DiscordObject {
	public:
		ActivityTimestamp() {}
		~ActivityTimestamp() {}

		inline bool empty() const {
			return !(start || end);
		}

		size_t start;
		size_t end;
	};

	void to_json(json& to, const ActivityTimestamp& from);
	void from_json(const json& from, ActivityTimestamp& to);


	// https://discordapp.com/developers/docs/topics/gateway#activity-object-activity-party
	struct ActivityParty : public DiscordObject {
	public:
		ActivityParty() {}
		~ActivityParty() {}

		inline bool empty() const {
			return ID.empty() && size.empty();
		}

		ActivityParty& operator=(const ActivityParty& rhs) {
			this->ID = rhs.ID;
			this->size = rhs.size;
			this->currentSize = rhs.size[0];
			this->maxSize = rhs.size[1];
			return *this;
		}

		std::string ID;
		std::array<long, 2> size;
		long& currentSize = size[0];
		long& maxSize = size[1];
	};

	void to_json(json& to, const ActivityParty& from);
	void from_json(const json& from, ActivityParty& to);

	// https://discordapp.com/developers/docs/topics/gateway#activity-object-activity-assets
	struct ActivityAssets : public DiscordObject {
	public:
		ActivityAssets() {}
		~ActivityAssets() {}

		inline bool empty() const {
			return largeImage.empty() && largeText.empty() && smallImage.empty() && smallText.empty();
		}

		std::string largeImage;
		std::string largeText;
		std::string smallImage;
		std::string smallText;
	};

	void to_json(json& to, const ActivityAssets& from);
	void from_json(const json& from, ActivityAssets& to);


	// https://discordapp.com/developers/docs/topics/gateway#activity-object-activity-secrets
	struct ActivitySecrets : public DiscordObject {
	public:
		ActivitySecrets() {}
		~ActivitySecrets() {}

		inline bool empty() const {
			return join.empty() && spectate.empty() && match.empty();
		}

		std::string join;
		std::string spectate;
		std::string match;
	};

	void to_json(json& to, const ActivitySecrets& from);
	void from_json(const json& from, ActivitySecrets& to);


	struct Emoji;

	// https://discordapp.com/developers/docs/topics/gateway#activity-object-activity-emoji
	struct ActivityEmoji : public DiscordObject {
	public:
		ActivityEmoji() {}
		~ActivityEmoji() {}

		inline bool empty() const {
			return name.empty();
		}

		std::string name;
		Snowflake<Emoji> ID;
		bool animated;
	};

	void to_json(json& to, const ActivityEmoji& from);
	void from_json(const json& from, ActivityEmoji& to);


	//This is here for the snowflake
	struct Application {};

	// https://discordapp.com/developers/docs/topics/gateway#activity-object
	struct Activity : public DiscordObject {
	public:
		Activity() {}
		~Activity() {}

		inline bool empty() const {
			return name.empty() || type == ACTIVITY_TYPE_NONE;
		}

		std::string name = "";
		enum ActivityType {
			ACTIVITY_TYPE_NONE = -1, // Set when null or nothing is passed.
			GAME = 0,
			STREAMING = 1,
			LISTENING = 2,
			CUSTOM = 3
		} type = ACTIVITY_TYPE_NONE;
		std::string url;
		size_t createdAt = 0;
		ActivityTimestamp timestamps;
		Snowflake<Application> applicationID; // The game's ID, for example.
		std::string details;
		std::string state;
		ActivityEmoji emoji; // For custom status
		ActivityParty party;
		ActivityAssets assets;
		ActivitySecrets secrets;
		bool instance;
		enum ActivityFlags {
			NONE = 0 << 0,
			INSTANCE = 1 << 0,
			JOIN = 1 << 1,
			SPECTATE = 1 << 2,
			JOIN_REQUEST = 1 << 3,
			SYNC = 1 << 4,
			PLAY = 1 << 5
		} flags = NONE;
	};


	void to_json(json& to, const Activity& from);
	void from_json(const json& from, Activity& to);

	// https://discordapp.com/developers/docs/topics/gateway#client-status-object
	struct ClientStatus : public DiscordObject {
	public:
		ClientStatus() {}
		~ClientStatus() {}

		inline bool empty() const {
			return desktop.empty() && mobile.empty() && web.empty();
		}

		std::string desktop;
		std::string mobile;
		std::string web;
	};

	void to_json(json& to, const ClientStatus& from);
	void from_json(const json& from, ClientStatus& to);


	// https://discordapp.com/developers/docs/topics/gateway#presence-update
	struct PresenceUpdate : public DiscordObject {
	public:
		PresenceUpdate() {}
		~PresenceUpdate() {}

		inline bool empty() const {
			return user.empty();
		}

		User user;
		std::vector<Snowflake<Role>> roleIDs;
		Activity game;
		std::string serverID; // Include loop, convert to Snowflake<Server>.
		std::string status;
		std::vector<Activity> activities;
		ClientStatus clientStatus;
		std::string boostedAt;
		std::string nick;
	};

	void to_json(json& to, const PresenceUpdate& from);
	void from_json(const json& from, PresenceUpdate& to);


	// https://discordapp.com/developers/docs/topics/gateway#update-status-gateway-status-update-structure
	struct StatusUpdate : public DiscordObject {
	public:
		StatusUpdate() {}
		~StatusUpdate() {}

		inline bool empty() const {
			return status == INVALID;
		}

		size_t idleSince = 0;
		Activity game;
		Status status = ONLINE;
		bool afk = false;

		const static std::vector<std::string> strings;
	};

	void to_json(json& to, const StatusUpdate& from);
	void from_json(const json& from, StatusUpdate& to);
}