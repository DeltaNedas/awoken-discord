#pragma once
#include <string>

#include "cache.h"
#include "channel.h"
#include "discord_object_interface.h"
#include "emoji.h"
#include "gateway.h"
#include "snowflake.h"
#include "user.h"
#include "voice.h"

namespace AwokenDiscord {
	enum Permission : long;
	struct Role;

	// https://discordapp.com/developers/docs/resources/guild#guild-member-object
	struct ServerMember : public IdentifiableDiscordObject<User> {
		ServerMember() {}
		~ServerMember() {}

		User user;
		std::string nick;
		std::vector<Snowflake<Role>> roles;
		std::string joinedAt;
		std::string boostedAt;
		bool deaf = false;
		bool mute = false;

		inline operator User&() {
			return user;
		}
	};

	void to_json(json& to, const ServerMember& from);
	void from_json(const json& from, ServerMember& to);


	// https://discordapp.com/developers/docs/resources/guild#guild-embed-object
	struct ServerEmbed : public DiscordObject {
		ServerEmbed() {}
		~ServerEmbed() {}

		bool enabled;
		Snowflake<Channel> channelID;
	};

	void to_json(json& to, const ServerEmbed& from);
	void from_json(const json& from, ServerEmbed& to);


	struct Application;
	struct PresenceUpdate;

	// https://discordapp.com/developers/docs/resources/guild#guild-object-guild-structure
	struct Server : public IdentifiableDiscordObject<Server> {
		Server() {};
		~Server() {}

		std::string name;
		std::string icon;
		std::string splash;
		bool isOwner = false; // if (ownerID == client.getID()) shorthand
		Snowflake<User> ownerID;
		Permission permissions; // Permissions of bot, **before** any channels
		std::string region;
		std::string afkChannelID;
		int afkTimeout = 0;
		bool embedEnabled = false;
		Snowflake<Channel> embedChannelID;
		int verificationLevel = 0;
		int defaultMessageNotifications = 0;
		int explicitContentFilter = 0;
		std::list<Role> roles;
		std::list<Emoji> emojis;
		std::list<std::string> features; // Nitro boost perks, verified, etc.
		int mfaLevel = 0;
		Snowflake<Application> applicationID; // If a bot created this server, who is it?
		bool widgetEnabled = false;
		Snowflake<Channel> widgetChannelID;
		Snowflake<Channel> systemChannelID;
		std::string joinedAt; // When bot joined this server

		// Only provided by onServer event, save them if you wish.
		bool large = false;
		bool unavailable = false;
		// size_t memberCount; // Provided by members.size();
		std::list<VoiceState> voiceStates;
		std::list<ServerMember> members;
		std::list<Channel> channels;
		std::list<PresenceUpdate> presences;

		// Back to normal
		size_t maxPresences = 5000; // If null, it is in effect.
		size_t maxMembers = 0;
		std::string vanityInvite; // discord.gg/vanityname
		std::string description;
		std::string banner;
		int boostTier = 0; // Can be gotten from boosters as well: 2 = 1, 15 = 2, 30 = 3
		size_t boosters = 0;
		std::string preferredLocale = "en-US";

		// Helpers
		std::list<ServerMember>::iterator findMember(const Snowflake<User> userID);
		std::list<Channel>::iterator findChannel(const Snowflake<Channel> channelID);
		std::list<Role>::iterator findRole(const Snowflake<Role> roleID);
	};

	void to_json(json& to, const Server& from);
	void from_json(const json& from, Server& to);


	class ServerCache : public Cache<Server> {
	public:
		using Cache<Server>::Cache;
		ServerCache() : Cache() {} // For some odd reason the default constructor isn't inherited (?)
		ServerCache(Cache<Server> list) : Cache<Server>(list) {}

		/*
		//Linear time complexity if unordered map: to do figure out how to do this with constant time complexity
		template <class Container, class json>
		iterator findOneWithJson(Container Server::*list, const Snowflake<json>& objectID) {
			return std::find_if(begin(), end(), [&objectID, list](Server& server) {
				auto result = objectID.findObject(server.*list);
				return result != std::end(server.*list);
			});
		}
		*/

		inline const_iterator findSeverWith(const Snowflake<Channel>& channelID) {
			return findOneWithJson(&Server::channels, channelID);
		}

		inline const_iterator findServerWith(const Snowflake<Role> roleID) {
			return findOneWithJson(&Server::roles, roleID);
		}

		//Linear time complexity if using list
		//Usually Constant time complexity if using unordered maps
		inline iterator findServer(const Snowflake<Server> serverID) {
			return serverID.findObject(*this);
		}
	};
}