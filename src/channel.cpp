#include "channel.h"

namespace AwokenDiscord {
	Overwrite::Overwrite(std::string type, Permission allow, Permission deny) {
		this->type = type;
		this->allow = allow;
		this->deny = deny;
	}

	// All fields are required, helpers are not needed.
	void to_json(json& to, const Overwrite& from) {
		to = {
			{"id", from.ID},
			{"type", from.type},
			{"allow", from.allow},
			{"deny", from.deny}
		};
	}
	void from_json(const json& from, Overwrite& to) {
		from["id"].get_to(to.ID);
		from["type"].get_to(to.type);
		from["allow"].get_to(to.allow);
		from["deny"].get_to(to.deny);
	}

	void to_json(json& to, const Channel& from) {
		addJsonField(to, "id", from.ID, REQUIRED);
		addJsonField(to, "type", fromEnum(from.type), REQUIRED);
		addJsonField(to, "guild_id", from.serverID);
		addJsonField(to, "position", from.position);
		addJsonField(to, "permission_overwrites", from.permissionOverwrites);
		addJsonField(to, "name", from.name);
		addJsonField(to, "topic", from.topic, OPTIONAL_NULLABLE);
		addJsonField(to, "nsfw", from.nsfw);
		addJsonField(to, "last_message_id", from.lastMessageID, OPTIONAL_NULLABLE);
		addJsonField(to, "bitrate", from.bitrate);
		addJsonField(to, "user_limit", from.userLimit);
		addJsonField(to, "recipients", from.recipients);
		addJsonField(to, "icon", from.icon, OPTIONAL_NULLABLE);
		addJsonField(to, "owner_id", from.ownerID);
		addJsonField(to, "parent_id", from.parentID, OPTIONAL_NULLABLE);
		addJsonField(to, "last_pin_timestamp", from.lastPinTimestamp);
	}
	void from_json(const json& from, Channel& to) {
		long type;
		getJsonField(from, "id", to.ID, REQUIRED);
		getJsonField(from, "type", type, REQUIRED);
		getJsonField(from, "guild_id", to.serverID);
		getJsonField(from, "position", to.position);
		getJsonField(from, "permission_overwrites", to.permissionOverwrites);
		getJsonField(from, "name", to.name);
		getJsonField(from, "topic", to.topic, OPTIONAL_NULLABLE);
		getJsonField(from, "nsfw", to.nsfw);
		getJsonField(from, "last_message_id", to.lastMessageID, OPTIONAL_NULLABLE);
		getJsonField(from, "bitrate", to.bitrate);
		getJsonField(from, "user_limit", to.userLimit);
		getJsonField(from, "recipients", to.recipients);
		getJsonField(from, "icon", to.icon, OPTIONAL_NULLABLE);
		getJsonField(from, "owner_id", to.ownerID);
		getJsonField(from, "parent_id", to.parentID, OPTIONAL_NULLABLE);
		getJsonField(from, "last_pin_timestamp", to.lastPinTimestamp);
		to.type = toEnum<Channel::ChannelType>(type);
	}
}