#include "embed.h"

namespace AwokenDiscord {
	void to_json(json& to, const EmbedThumbnail& from) {
		addJsonField(to, "url", from.url);
		addJsonField(to, "proxy_url", from.proxyUrl);
		addJsonField(to, "height", from.height);
		addJsonField(to, "width", from.width);
	}
	void from_json(const json& from, EmbedThumbnail& to) {
		getJsonField(from, "url", to.url);
		getJsonField(from, "proxy_url", to.proxyUrl);
		getJsonField(from, "height", to.height);
		getJsonField(from, "width", to.width);
	}

	void to_json(json& to, const EmbedVideo& from) {
		addJsonField(to, "url", from.url);
		addJsonField(to, "height", from.height);
		addJsonField(to, "width", from.width);
	}
	void from_json(const json& from, EmbedVideo& to) {
		getJsonField(from, "url", to.url);
		getJsonField(from, "height", to.height);
		getJsonField(from, "width", to.width);
	}

	void to_json(json& to, const EmbedImage& from) {
		addJsonField(to, "url", from.url);
		addJsonField(to, "proxy_url", from.proxyUrl);
		addJsonField(to, "height", from.height);
		addJsonField(to, "width", from.width);
	}
	void from_json(const json& from, EmbedImage& to) {
		getJsonField(from, "url", to.url);
		getJsonField(from, "proxy_url", to.proxyUrl);
		getJsonField(from, "height", to.height);
		getJsonField(from, "width", to.width);
	}

	void to_json(json& to, const EmbedProvider& from) {
		addJsonField(to, "name", from.name);
		addJsonField(to, "url", from.url);
	}
	void from_json(const json& from, EmbedProvider& to) {
		getJsonField(from, "name", to.name);
		getJsonField(from, "url", to.url);
	}

	void to_json(json& to, const EmbedAuthor& from) {
		addJsonField(to, "name", from.name);
		addJsonField(to, "url", from.url);
		addJsonField(to, "icon_url", from.iconUrl);
		addJsonField(to, "proxy_icon_url", from.proxyIconUrl);
	}
	void from_json(const json& from, EmbedAuthor& to) {
		getJsonField(from, "name", to.name);
		getJsonField(from, "url", to.url);
		getJsonField(from, "icon_url", to.iconUrl);
		getJsonField(from, "proxy_icon_url", to.proxyIconUrl);
	}

	void to_json(json& to, const EmbedFooter& from) {
		addJsonField(to, "text", from.text, REQUIRED);
		addJsonField(to, "icon_url", from.iconUrl);
		addJsonField(to, "proxy_icon_url", from.proxyIconUrl);
	}
	void from_json(const json& from, EmbedFooter& to) {
		getJsonField(from, "text", to.text, REQUIRED);
		getJsonField(from, "icon_url", to.iconUrl);
		getJsonField(from, "proxy_icon_url", to.proxyIconUrl);
	}

	void to_json(json& to, const EmbedField& from) {
		addJsonField(to, "name", from.name, REQUIRED);
		addJsonField(to, "value", from.value, REQUIRED);
		addJsonField(to, "inline", from.isInline);
	}
	void from_json(const json& from, EmbedField& to) {
		getJsonField(from, "name", to.name, REQUIRED);
		getJsonField(from, "value", to.value, REQUIRED);
		getJsonField(from, "inline", to.isInline);
	}

	void to_json(json& to, const Embed& from) {
		addJsonField(to, "title", from.title);
		addJsonField(to, "type", from.type);
		addJsonField(to, "description", from.description);
		addJsonField(to, "url", from.url);
		addJsonField(to, "timestamp", from.timestamp);
		addJsonField(to, "color", from.color, OPTIONAL, -1);
		addJsonField(to, "footer", from.footer);
		addJsonField(to, "image", from.image);
		addJsonField(to, "thumbnail", from.thumbnail);
		addJsonField(to, "video", from.video);
		addJsonField(to, "provider", from.provider);
		addJsonField(to, "author", from.author);
		addJsonField(to, "fields", from.fields);
	}
	void from_json(const json& from, Embed& to) {
		getJsonField(from, "title", to.title);
		getJsonField(from, "type", to.type);
		getJsonField(from, "description", to.description);
		getJsonField(from, "url", to.url);
		getJsonField(from, "timestamp", to.timestamp);
		getJsonField(from, "color", to.color);
		getJsonField(from, "footer", to.footer);
		getJsonField(from, "image", to.image);
		getJsonField(from, "thumbnail", to.thumbnail);
		getJsonField(from, "video", to.video);
		getJsonField(from, "provider", to.provider);
		getJsonField(from, "author", to.author);
		getJsonField(from, "fields", to.fields);
	}
}