#include "webhook.h"

namespace AwokenDiscord {
	void to_json(json& to, const Webhook& from) {
		addJsonField(to, "id", from.ID, REQUIRED);
		addJsonField(to, "type", from.type, REQUIRED);
		addJsonField(to, "guild_id", from.serverID);
		addJsonField(to, "channel_id", from.channelID, REQUIRED);
		addJsonField(to, "user", from.user);
		addJsonField(to, "name", from.name, NULLABLE);
		addJsonField(to, "avatar", from.avatar, NULLABLE);
		addJsonField(to, "token", from.token);
	}
	void from_json(const json& from, Webhook& to) {
		getJsonField(from, "id", to.ID, REQUIRED);
		getJsonField(from, "type", to.type, REQUIRED);
		getJsonField(from, "guild_id", to.serverID);
		getJsonField(from, "channel_id", to.channelID, REQUIRED);
		getJsonField(from, "user", to.user);
		getJsonField(from, "name", to.name, NULLABLE);
		getJsonField(from, "avatar", to.avatar, NULLABLE);
		getJsonField(from, "token", to.token);
	}
}
