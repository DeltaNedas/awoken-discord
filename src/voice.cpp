#include "voice.h"

namespace AwokenDiscord {
	void to_json(json& to, const VoiceState& from) {
		addJsonField(to, "guild_id", from.serverID);
		addJsonField(to, "channel_id", from.channelID, NULLABLE);
		addJsonField(to, "user_id", from.userID, REQUIRED);
		addJsonField(to, "session_id", from.sessionID, REQUIRED);
		addJsonField(to, "deaf", from.deaf, REQUIRED);
		addJsonField(to, "mute", from.mute, REQUIRED);
		addJsonField(to, "self_deaf", from.selfDeaf, REQUIRED);
		addJsonField(to, "self_mute", from.selfMute, REQUIRED);
		addJsonField(to, "suppress", from.suppress, REQUIRED);
	}
	void from_json(const json& from, VoiceState& to) {
		getJsonField(from, "guild_id", to.serverID);
		getJsonField(from, "channel_id", to.channelID, NULLABLE);
		getJsonField(from, "user_id", to.userID, REQUIRED);
		getJsonField(from, "session_id", to.sessionID, REQUIRED);
		getJsonField(from, "deaf", to.deaf, REQUIRED);
		getJsonField(from, "mute", to.mute, REQUIRED);
		getJsonField(from, "self_deaf", to.selfDeaf, REQUIRED);
		getJsonField(from, "self_mute", to.selfMute, REQUIRED);
		getJsonField(from, "suppress", to.suppress, REQUIRED);
	}

	void to_json(json& to, const VoiceRegion& from) {
		to = {
			{"id", from.ID},
			{"name", from.name},
			{"vip", from.vip},
			{"optimal", from.optimal},
			{"deprecated", from.deprecated},
			{"custom", from.custom}
		};
	}
	void from_json(const json& from, VoiceRegion& to) {
		from["id"].get_to(to.ID);
		from["name"].get_to(to.name);
		from["vip"].get_to(to.vip);
		from["optimal"].get_to(to.optimal);
		from["deprecated"].get_to(to.deprecated);
		from["custom"].get_to(to.custom);
	}

	void to_json(json& to, const VoiceServerUpdate& from) {
		to = {
			{"token", from.token},
			{"guild_id", from.serverID},
			{"endpoint", from.endpoint}
		};
	}
	void from_json(const json& from, VoiceServerUpdate& to) {
		from["token"].get_to(to.token);
		from["guild_id"].get_to(to.serverID);
		from["endpoint"].get_to(to.endpoint);
	}
}
