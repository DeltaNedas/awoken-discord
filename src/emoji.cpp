#include "emoji.h"
#include <iostream>

namespace AwokenDiscord {
	void to_json(json& to, const Emoji& from) {
		addJsonField(to, "id", from.ID, NULLABLE);
		addJsonField(to, "name", from.name, NULLABLE);
		addJsonField(to, "roles", from.roles);
		addJsonField(to, "user", from.user);
		addJsonField(to, "require_colons", from.requireColons);
		addJsonField(to, "managed", from.managed);
		addJsonField(to, "animated", from.animated);
	}
	void from_json(const json& from, Emoji& to) {
		getJsonField(from, "id", to.ID, NULLABLE);
		getJsonField(from, "name", to.name, NULLABLE);
		getJsonField(from, "roles", to.roles);
		getJsonField(from, "user", to.user);
		getJsonField(from, "require_colons", to.requireColons);
		getJsonField(from, "managed", to.managed);
		getJsonField(from, "animated", to.animated);
	}

	// All fields are required so helper functions are not needed.
	void to_json(json& to, const Reaction& from) {
		to = {
			{"count", from.count},
			{"me", from.me},
			{"emoji", from.emoji}
		};
	}
	void from_json(const json& from, Reaction& to) {
		from["count"].get_to(to.count);
		from["me"].get_to(to.me);
		from["emoji"].get_to(to.emoji);
	}
}