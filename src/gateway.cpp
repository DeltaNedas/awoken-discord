#include "gateway.h"

#include "json_wrapper.h"

namespace AwokenDiscord {
	void to_json(json& to, const UnavailableServer& from) {
		to = {
			{"id", from.ID}
		};
	}
	void from_json(const json& from, UnavailableServer& to) {
		to.ID = from["id"];
	}

	void to_json(json& to, const Ready& from) {
		addJsonField(to, "v", from.v, REQUIRED);
		addJsonField(to, "user", from.user, REQUIRED);
		addJsonField(to, "private_channels", from.privateChannels, REQUIRED);
		addJsonField(to, "guilds", from.servers, REQUIRED);
		addJsonField(to, "session_id", from.sessionID, REQUIRED);
		addJsonField(to, "shard", from.shard);
	}
	void from_json(const json& from, Ready& to) {
		getJsonField(from, "v", to.v, REQUIRED);
		getJsonField(from, "user", to.user, REQUIRED);
		getJsonField(from, "private_channels", to.privateChannels, REQUIRED);
		getJsonField(from, "guilds", to.servers, REQUIRED);
		getJsonField(from, "session_id", to.sessionID, REQUIRED);
		getJsonField(from, "shard", to.shard);
	}

	void to_json(json& to, const ActivityTimestamp& from) {
		addJsonField(to, "start", from.start);
		addJsonField(to, "end", from.end);
	}
	void from_json(const json& from, ActivityTimestamp& to) {
		getJsonField(from, "start", to.start);
		getJsonField(from, "end", to.end);
	}

	void to_json(json& to, const ActivityParty& from) {
		addJsonField(to, "id", from.ID);
		addJsonField(to, "size", from.size);
	}
	void from_json(const json& from, ActivityParty& to) {
		getJsonField(from, "id", to.ID);
		getJsonField(from, "size", to.size);
	}

	void to_json(json& to, const ActivityAssets& from) {
		addJsonField(to, "large_image", from.largeImage);
		addJsonField(to, "large_text", from.largeText);
		addJsonField(to, "small_image", from.smallImage);
		addJsonField(to, "small_text", from.smallText);
	}
	void from_json(const json& from, ActivityAssets& to) {
		getJsonField(from, "large_image", to.largeImage);
		getJsonField(from, "large_text", to.largeText);
		getJsonField(from, "small_image", to.smallImage);
		getJsonField(from, "small_text", to.smallText);
	}

	void to_json(json& to, const ActivitySecrets& from) {
		addJsonField(to, "join", from.join);
		addJsonField(to, "spectate", from.spectate);
		addJsonField(to, "match", from.match);
	}
	void from_json(const json& from, ActivitySecrets& to) {
		getJsonField(from, "join", to.join);
		getJsonField(from, "spectate", to.spectate);
		getJsonField(from, "match", to.match);
	}

	void to_json(json& to, const ActivityEmoji& from) {
		addJsonField(to, "name", from.name, REQUIRED);
		addJsonField(to, "id", from.ID);
		addJsonField(to, "animated", from.animated);
	}
	void from_json(const json& from, ActivityEmoji& to) {
		getJsonField(from, "name", to.name, REQUIRED);
		getJsonField(from, "id", to.ID);
		getJsonField(from, "animated", to.animated);
	}

	void to_json(json& to, const Activity& from) {
		addJsonField(to, "name", from.name, REQUIRED);
		addJsonField(to, "type", fromEnum(from.type), REQUIRED);
		addJsonField(to, "url", from.url, OPTIONAL_NULLABLE);
		addJsonField(to, "created_at", from.createdAt, REQUIRED);
		addJsonField(to, "timestamps", from.timestamps);
		addJsonField(to, "application_id", from.applicationID);
		addJsonField(to, "details", from.details, OPTIONAL_NULLABLE);
		addJsonField(to, "state", from.state, OPTIONAL_NULLABLE);
		addJsonField(to, "emoji", from.emoji, OPTIONAL_NULLABLE);
		addJsonField(to, "party", from.party);
		addJsonField(to, "assets", from.assets);
		addJsonField(to, "secrets", from.secrets);
		addJsonField(to, "instance", from.instance);
		addJsonField(to, "flags", fromEnum(from.flags));
	}
	void from_json(const json& from, Activity& to) {
		long type, flags;
		getJsonField(from, "name", to.name, REQUIRED);
		getJsonField(from, "type", type, REQUIRED);
		getJsonField(from, "url", to.url, OPTIONAL_NULLABLE);
		getJsonField(from, "created_at", to.createdAt, REQUIRED);
		getJsonField(from, "timestamps", to.timestamps);
		getJsonField(from, "application_id", to.applicationID);
		getJsonField(from, "details", to.details, OPTIONAL_NULLABLE);
		getJsonField(from, "state", to.state, OPTIONAL_NULLABLE);
		getJsonField(from, "emoji", to.emoji, OPTIONAL_NULLABLE);
		getJsonField(from, "party", to.party);
		getJsonField(from, "assets", to.assets);
		getJsonField(from, "secrets", to.secrets);
		getJsonField(from, "instance", to.instance);
		getJsonField(from, "flags", flags);
		to.type = toEnum<Activity::ActivityType>(type);
		to.flags = toEnum<Activity::ActivityFlags>(flags);
	}

	void to_json(json& to, const ClientStatus& from) {
		addJsonField(to, "desktop", from.desktop);
		addJsonField(to, "mobile", from.mobile);
		addJsonField(to, "web", from.web);
	}
	void from_json(const json& from, ClientStatus& to) {
		getJsonField(from, "desktop", to.desktop);
		getJsonField(from, "mobile", to.mobile);
		getJsonField(from, "web", to.web);
	}

	void to_json(json& to, const PresenceUpdate& from) {
		addJsonField(to, "user", from.user, REQUIRED);
		addJsonField(to, "roles", from.roleIDs, REQUIRED);
		addJsonField(to, "game", from.game, NULLABLE);
		addJsonField(to, "guild_id", from.serverID, REQUIRED);
		addJsonField(to, "status", from.status, REQUIRED);
		addJsonField(to, "activities", from.activities, REQUIRED);
		addJsonField(to, "client_status", from.clientStatus, REQUIRED);
		addJsonField(to, "premium_since", from.boostedAt, OPTIONAL_NULLABLE);
		addJsonField(to, "nick", from.nick, OPTIONAL_NULLABLE);
	}
	void from_json(const json& from, PresenceUpdate& to) {
		getJsonField(from, "user", to.user, REQUIRED);
		getJsonField(from, "roles", to.roleIDs); // Not included for GUILD_CREATE in presences
		getJsonField(from, "game", to.game, NULLABLE);
		getJsonField(from, "guild_id", to.serverID); // ^^^^
		getJsonField(from, "status", to.status, REQUIRED);
		getJsonField(from, "activities", to.activities, REQUIRED);
		getJsonField(from, "client_status", to.clientStatus, REQUIRED);
		getJsonField(from, "premium_since", to.boostedAt, OPTIONAL_NULLABLE);
		getJsonField(from, "nick", to.nick, OPTIONAL_NULLABLE);
	}

	void to_json(json& to, const StatusUpdate& from) {
		addJsonField(to, "since", from.idleSince, NULLABLE);
		addJsonField(to, "game", from.game, NULLABLE);
		addJsonField(to, "status", from.strings[from.status]);
		addJsonField(to, "afk", from.afk);
	}
	void from_json(const json& from, StatusUpdate& to) {
		std::string status;
		getJsonField(from, "since", to.idleSince, NULLABLE);
		getJsonField(from, "game", to.game, NULLABLE);
		getJsonField(from, "status", status);
		getJsonField(from, "afk", to.afk);

		to.status = INVALID;
		for (size_t i = 0; i < to.strings.size(); i++) {
			if (status == to.strings[i]) {
				to.status = toEnum<Status>(i);
			}
		}
	}

	const std::vector<std::string> StatusUpdate::strings = {
		"online", "dnd", "idle", "invisible", "offline"
	};
}