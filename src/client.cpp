#if _MSC_VER && !__INTEL_COMPILER
#	pragma warning( disable: 4307 ) // Ignore integer overflow, becuase we are taking advantage of it
#endif

#include <chrono>
#include <cstring>
#include <ctime>
#include <functional>
#include <iomanip>
#include <sstream>

#include "client.h"
#include "json_wrapper.h"
#include "version_helper.h"

namespace AwokenDiscord {
	void BaseDiscordClient::start(const std::string _token, const char maxNumOfThreads, int _shardID, int _shardCount) {
		if (!scheduleHandler) {
			setError(CANT_SCHEDULE);
			return;
		}

		ready = false;
		quitting = false;
		bot = true;
		token = std::unique_ptr<std::string>(new std::string(_token)); //add client to list
		if (_shardID != 0 || _shardCount != 0)
			setShardID(_shardID, _shardCount);

		messagesRemaining = 4;
		if (debugLevel & DebugLevel::DEBUG) {
			std::cout << "[DEBUG] Getting gateway...\n";
		}
		getTheGateway();
		if (debugLevel & DebugLevel::DEBUG) {
			std::cout << "[DEBUG] Connecting to gateway...\n";
		}
		connect(theGateway, this, connection);
		if (debugLevel & DebugLevel::DEBUG) {
			std::cout << "[DEBUG] Started!\n";
		}
#ifndef AWOKEN_ONE_THREAD
		if (USE_RUN_THREAD <= maxNumOfThreads) runAsync();
#endif
	}

	BaseDiscordClient::~BaseDiscordClient() {
		ready = false;
		if (heart.isValid()) heart.stop();
	}

	Response BaseDiscordClient::request(const RequestMethod method, Route path, const std::string jsonParameters/*,
		cpr::Parameters httpParameters*/, const std::initializer_list<Part>& multipartParameters,
		RequestCallback callback, RequestMode mode) {
		//check if rate limited
		Response response;
		const time_t currentTime = getEpochTimeMillisecond();
		response.birth = currentTime;
		const auto continueBeingRateLimited = [&]() {
			onExceededRateLimit(isGlobalRateLimited, nextRetry - currentTime, mode, { *this, method, path, jsonParameters, multipartParameters, callback });
			response.statusCode = TOO_MANY_REQUESTS;
			setError(response.statusCode);
			return response;
		};
		if (isGlobalRateLimited) {
			if (nextRetry <= currentTime) {
				isGlobalRateLimited = false;
			} else {
				return continueBeingRateLimited();
			}
		}
		const std::string bucket = path.bucket(method);
		auto bucketResetTimestamp = buckets.find(bucket);
		if (bucketResetTimestamp != buckets.end()) {
			if (bucketResetTimestamp->second <= currentTime) {
				buckets.erase(bucketResetTimestamp);
			} else {
				return continueBeingRateLimited();
			}
		}
		{	//the { is used so that onResponse is called after session is removed to make debugging performance issues easier
			//request starts here
			Session session;
			session.setUrl("https://discordapp.com/api/v6/" + path.url());
			std::vector<HeaderPair> header = {
				{ "Authorization", bot ? "Bot " + getToken() : getToken() },
				{ "User-Agent", userAgent },
			};
			if (jsonParameters != "") {
				session.setBody(&jsonParameters);
				header.push_back({ "Content-Type", "application/json" });
				header.push_back({ "Content-Length", std::to_string(jsonParameters.size()) });
			//} else if (httpParameters.content .size()) { // TODO: Fix this
			//	session.SetParameters(httpParameters);
			} else if (0 < multipartParameters.size()) {
				session.setMultipart(multipartParameters);
				header.push_back({ "Content-Type", "multipart/form-data" });
			} else {
				header.push_back({ "Content-Length", "0" });
			}
			session.setHeader(header);

			//Do the response
			switch (method) {
			case Post: case Patch: case Delete: case Get: case Put:
				response = session.request(method);
				break;
			default: response.statusCode = BAD_REQUEST; break; //unexpected method
			}

			//status checking
			switch (response.statusCode) {
			case OK: case CREATED: case NO_CONTENT: case NOT_MODIFIED: break;
			case TOO_MANY_REQUESTS: { // This should fall down to default
					std::string rawRetryAfter = response.header["Retry-After"];
					// Use retry after or 5 seconds (arbitrary)
					int retryAfter = rawRetryAfter.size() ? std::stoi(rawRetryAfter) : 5000;
					isGlobalRateLimited = response.header["X-RateLimit-Global"] == "true";
					nextRetry = getEpochTimeMillisecond() + retryAfter;
					if (!isGlobalRateLimited) {
						buckets[bucket] = nextRetry;
						onDepletedRequestSupply(bucket, retryAfter);
					}
					onExceededRateLimit(isGlobalRateLimited, retryAfter, mode, { *this, method, path, jsonParameters, multipartParameters, callback });
				}
			default: { // Error
					const ErrorCode code = static_cast<ErrorCode>(response.statusCode);
					setError(code);
					json data = json::parse(response.text);
					auto errorCode = data.find("code");
					auto errorMessage = data.find("message");
					if (errorCode != data.end()) {
						onError(static_cast<ErrorCode>(errorCode.value().get<int>()),
							errorMessage != data.end() ? errorMessage.value().get<std::string>() : "");
					} else if (response.text.size()) {
						onError(ERROR_NOTE, response.text);
					}
#if defined(__cpp_exceptions) || defined(__EXCEPTIONS)
					throw code;
#endif
				}
			}

			//rate limit check
			if (response.header["X-RateLimit-Remaining"] == "0" && response.statusCode != TOO_MANY_REQUESTS) {
				std::tm date = {};
				//for some reason std::get_time requires gcc 5
				std::istringstream dateStream(response.header["Date"]);
				dateStream >> std::get_time(&date, "%a, %d %b %Y %H:%M:%S GMT");
				const time_t reset = std::stoi(response.header["X-RateLimit-Reset"]);
#if defined(_WIN32) || defined(_WIN64)
				std::tm gmTM;
				std::tm*const resetGM = &gmTM;
				gmtime_s(resetGM, &reset);
#else
				std::tm* resetGM = std::gmtime(&reset);
#endif
				const time_t resetDelta = (std::mktime(resetGM) - std::mktime(&date)) * 1000;
				buckets[bucket] = resetDelta + getEpochTimeMillisecond();
				onDepletedRequestSupply(bucket, resetDelta);
			}

			if (callback)
				callback(response);
		}
		onResponse(response);
		return response;
	}

	const Route BaseDiscordClient::path(const char * source, std::initializer_list<std::string> values) {
		return Route(source, values);
	}

	std::shared_ptr<ServerCache> BaseDiscordClient::createServerCache() {
		setServerCache(std::make_shared<ServerCache>());
		return getServerCache();
	}

	void BaseDiscordClient::setServerCache(std::shared_ptr<ServerCache> cache) {
		serverCache = cache;
		if ((ready || !isBot()) && serverCache->size() == 0)
			*serverCache = getServers().get<Cache>();
	}

	void BaseDiscordClient::onDepletedRequestSupply(const Route::Bucket&, time_t) {
	}

	void BaseDiscordClient::onExceededRateLimit(bool, std::time_t timeTilRetry, RequestMode mode, Request request) {
		if (mode == Async)
			schedule(request, timeTilRetry);
	}


	void BaseDiscordClient::updateStatus(StatusUpdate update) {
		sendL(json{
			{"op", (int) STATUS_UPDATE},
			{"d", update}
		}.dump());
	}

	void BaseDiscordClient::waitTilReady() {
		while (!ready) sleep(1000);
	}

	void BaseDiscordClient::setShardID(int _shardID, int _shardCount) {
		shardID = _shardID;
		shardCount = _shardCount;
	}

	void BaseDiscordClient::getTheGateway() {
#if USE_HARD_CODED_GATEWAY
		theGateway = "wss://gateway.discord.gg/?v=6";	//This is needed for when session is disabled
#else
		Session session;
		session.setUrl("https://discordapp.com/api/gateway");
		Response a = session.request(Get); // TODO: Change this to POST
		if (a.text.empty()) { // Error check
			quit(false, true);
			return setError(GATEWAY_FAILED);
		}
		//getting the gateway
		for (unsigned int position = 0, j = 0; ; ++position) {
			if (a.text[position] == '"')
				++j;
			else if (j == 3) {
				const unsigned int start = position;
				while (a.text[++position] != '"');
				unsigned int size = position - start;
				theGateway.reserve(32);
				theGateway.append(a.text, start, size);
				theGateway += "/?v=6";
				break;
			}
		}
#endif
	}

	// https://discordapp.com/developers/docs/topics/gateway#identify-identify-structure
	void BaseDiscordClient::sendIdentity(std::string os, bool compress, size_t largeThreshold, StatusUpdate initialStatus, bool presenceEvents) {
		std::string browser = os;
#if !HIDE_OS
	browser = "Awoken_Discord";
#	if defined(_WIN32) || defined(_WIN64)
		os = "Windows";
#	elif defined(__APPLE__) || defined(__MACH__)
		os = "macOS";
#	elif defined(__linux__) || defined(linux) || defined(__linux)
		os = "Linux"; // It's GANOOO/linocks..
#	elif defined __FreeBSD__
		os = "FreeBSD";
#	elif defined(unix) || defined(__unix__) || defined(__unix)
		os = "Unix";
#	endif
#endif

		json identity = {
			{"op", 2},
			{"d", {
				{"token", getToken()},
				{"properties", {
					{"$os", os},
					{"$browser", browser},
					{"$device", browser}
				}},
				{"compress", compress},
				{"largeThreshold", largeThreshold},
				{"presence", initialStatus},
				{"guild_subscriptions", presenceEvents}
			}}
		};
		if (shardCount && shardCount >= shardID) {
			identity["id"]["shard"] = {
				shardID, shardCount
			};
		}

		sendL(identity.dump());
	}

	void BaseDiscordClient::scheduledSendIdentity() {
		sendIdentity();
	}

	void BaseDiscordClient::sendResume() {
		sendL(json{
			{"op", 6},
			{"d", {
				{"token", getToken()},
				{"session_id", sessionID},
				{"seq", lastSReceived}
			}}
		}.dump());
		onResume();
	}

	void BaseDiscordClient::quit(bool isRestarting, bool isDisconnected) {
		if (!isRestarting)
			quitting = true;

#if USE_VOICE
		//quit all voice connections
		for (VoiceConnection& voiceConnection : voiceConnections)
			voiceConnection.disconnect();
#endif
		if (heart.isValid()) heart.stop(); //stop heartbeating
		if (!isDisconnected) disconnectWebsocket(1000);
		stopClient();
		if (quitting) onQuit();
	}

	void BaseDiscordClient::restart() {
		quit(true);
		connect(theGateway, this, connection);
		onRestart();
	}

	void BaseDiscordClient::reconnect(const unsigned int status) {
		if (status != 1000) { //check for a deliberate reconnect
			heartbeatInterval = 0; //stop heartbeating
			wasHeartbeatAcked = true; //stops the library from spamming discord
		}
		disconnectWebsocket(status);
		if (consecutiveReconnectsCount == 10) getTheGateway();
		if (reconnectTimer.isValid())
			reconnectTimer.stop();
		reconnectTimer = schedule([this]() {
			connect(theGateway, this, connection);
		}, consecutiveReconnectsCount < 50 ? consecutiveReconnectsCount * 5000 : 5000 * 50);
		++consecutiveReconnectsCount;
	}

	void BaseDiscordClient::disconnectWebsocket(unsigned int code, const std::string reason) {
		disconnect(code, reason, connection);
		onDisconnect();
	}

	bool BaseDiscordClient::sendL(std::string message) {
		if (nextHalfMin <= getEpochTimeMillisecond()) {
			const unsigned int maxMessagesPerMin = 116;
			const unsigned int halfMinMilliseconds = 30000;
			const unsigned int maxMessagesPerHalfMin = maxMessagesPerMin / 2;

			nextHalfMin += halfMinMilliseconds;
			messagesRemaining = maxMessagesPerHalfMin;
		}

		if (--messagesRemaining < 0) {
			messagesRemaining = 0;
			setError(RATE_LIMITED);
			return false;
		}
		send(message, connection);
		return true;
	}

	void BaseDiscordClient::processMessage(const std::string &message) {
		json doc = json::parse(message);
		int op = doc["op"].get<int>();
		std::string t = ""; getJsonField(doc, "t", t, NULLABLE);
		json d; getJsonField(doc, "d", d, NULLABLE);
		try {
			switch (op) {
			case DISPATCH:
				lastSReceived = doc["s"].get<int>();
				consecutiveReconnectsCount = 0; //Successfully connected
				if (t == "READY") {
					if (debugLevel & DebugLevel::STATUS) {
						std::cout << "[STATUS] Bot is ready.\n";
					}

					Ready readyData = d;
					sessionID = readyData.sessionID;
					bot = readyData.user.bot;
					userID = readyData.user;
					this->onReady(readyData);
					ready = true;
				} else if (t == "RESUMED") { this->onResumed();
				} else if (t == "GUILD_CREATE") {
					Server server(d);
					if (serverCache)
						serverCache->insert(server);
					this->onServer(server);
				} else if (t == "GUILD_DELETE") {
					UnavailableServer server(d);
					if (serverCache) {
						findServerInCache(server.ID, [=, this](ServerCache::iterator& found) {
							serverCache->erase(found);
						});
					}
					this->onDeleteServer(server);
				} else if (t == "GUILD_UPDATE") {
					Server server(d);
					accessServerFromCache(server.ID, [server](Server& foundServer) {
						foundServer = server;
					});
					this->onEditServer(server);
				} else if (t == "GUILD_BAN_ADD") { onBan (d["guild_id"], d["user"]);
				} else if (t == "GUILD_BAN_REMOVE") { onUnban(d["guild_id"], d["user"]);
				} else if (t == "GUILD_INTEGRATIONS_UPDATE") {
					// TODO: Add this
				} else if (t == "GUILD_MEMBER_ADD") {
					Snowflake<Server> serverID = d["guild_id"];
					ServerMember member(d);
					appendjsonToCache(serverID, &Server::members, member);
					this->onMember(serverID, member);
				} else if (t == "GUILD_MEMBER_REMOVE") {
					Snowflake<Server> serverID = d["guild_id"];
					User user = d["user"];
					erasejsonFromCache(serverID, &Server::members, user.ID);
					this->onRemoveMember(serverID, user);
				} else if (t == "GUILD_MEMBER_UPDATE") {
					Snowflake<Server> serverID = d["guild_id"];
					User user = d["user"];
					std::vector<Snowflake<Role>> roles = d["roles"];
					const json& nickjson = d["nick"];
					std::string nick = nickjson.is_string() ? nickjson : "";
					accessjsonFromCache(serverID, &Server::members, user.ID,
						[user, roles, nick](Server&, ServerMember& member) {
							member.user = user;
							member.roles = roles;
							member.nick = nick;
						}
					);
					this->onEditMember(serverID, user, roles, nick);
				} else if (t == "GUILD_MEMBERS_CHUNK") {onMemberChunk (d["guild_id"], d["members"]);
				} else if (t == "GUILD_ROLE_CREATE") {
					Snowflake<Server> serverID = d["guild_id"];
					Role role = d["role"];
					appendjsonToCache(serverID, &Server::roles, role);
					onRole(serverID, role);
				} else if (t == "GUILD_ROLE_UPDATE") {
					Snowflake<Server> serverID = d["guild_id"];
					Role role = d["role"];
					accessjsonFromCache(serverID, &Server::roles, role.ID,
						[role](Server&, Role& foundRole) {
							foundRole = role;
						}
					);
					onEditRole(serverID, role);
				} else if (t == "GUILD_ROLE_DELETE") {
					Snowflake<Server> serverID = d["guild_id"];
					Snowflake<Role> roleID = d["role_id"];
					erasejsonFromCache(serverID, &Server::roles, roleID);
					onDeleteRole(serverID, roleID);
				} else if (t == "GUILD_EMOJIS_UPDATE") { onEditEmojis (d["guild_id"], d["emojis"]);
				} else if (t == "CHANNEL_CREATE") {
					Channel channel = d;
					appendjsonToCache(channel.serverID, &Server::channels, channel);
					onChannel(d);
				} else if (t == "CHANNEL_UPDATE") {
					Channel channel = d;
					accessjsonFromCache(channel.serverID, &Server::channels, channel.ID,
						[channel](Server&, Channel& foundChannel) {
							foundChannel = channel;
						}
					);
					onEditChannel(d);
				} else if (t == "CHANNEL_DELETE") {
					Channel channel = d;
					erasejsonFromCache(channel.serverID, &Server::channels, channel.ID);
					onDeleteChannel(d);
				} else if (t == "CHANNEL_PINS_UPDATE") {
					const json& lastPinJson = d["last_pin_timestamp"];
					onPinMessage(
						d["channel_id"],
						lastPinJson.is_string() ? lastPinJson : ""
					);
				} else if (t == "PRESENCE_UPDATE") { onPresenceUpdate (d);
				} else if (t == "PRESENCES_REPLACE") {
					// TODO: Look at this and see if it should be empty?
				} else if (t == "USER_UPDATE") { onEditUser (d);
				} else if (t == "USER_NOTE_UPDATE") { onEditUserNote (d);
				} else if (t == "USER_SETTINGS_UPDATE") { onEditUserSettings (d);
				} else if (t == "VOICE_STATE_UPDATE") {
					VoiceState state(d);
	#if USE_VOICE
					if (!waitingVoiceContexts.empty()) {
						auto iterator = find_if(waitingVoiceContexts.begin(), waitingVoiceContexts.end(),
							[&state](const VoiceContext* w) {
							return state.channelID == w->channelID && w->sessionID == "";
						});
						if (iterator != waitingVoiceContexts.end()) {
							VoiceContext& context = **iterator;
							context.sessionID = state.sessionID;
							connectToVoiceIfReady(context);
						}
					}
	#endif
					onEditVoiceState(state);
				} else if (t == "TYPING_START") { onTyping (d["channel_id"], d["user_id"], d["timestamp"].get<long>() * 1000);
				} else if (t == "MESSAGE_CREATE") { this->onMessage(d);
				} else if (t == "MESSAGE_UPDATE") { this->onEditMessage (d);
				} else if (t == "MESSAGE_DELETE") { this->onDeleteMessages (d["channel_id"], { d["id"] });
				} else if (t == "MESSAGE_DELETE_BULK") { onDeleteMessages (d["channel_id"], d["ids"]);
				} else if (t == "VOICE_SERVER_UPDATE") {
					VoiceServerUpdate voiceServer(d);
	#if USE_VOICE
					if (!waitingVoiceContexts.empty()) {
						auto iterator = find_if(waitingVoiceContexts.begin(), waitingVoiceContexts.end(),
							[&voiceServer](const VoiceContext* w) {
							return voiceServer.serverID == w->serverID && w->endpoint == "";
						});
						if (iterator != waitingVoiceContexts.end()) {
							VoiceContext& context = **iterator;
							context.token = voiceServer.token;
							context.endpoint = voiceServer.endpoint;
							connectToVoiceIfReady(context);
						}
					}
	#endif
					onEditVoiceServer(voiceServer);
				} else if (t == "GUILD_SYNC") { onServerSync (d);
				} else if (t == "RELATIONSHIP_ADD") {  onRelationship (d);
				} else if (t == "RELATIONSHIP_REMOVE") { onDeleteRelationship(d);
				} else if (t == "MESSAGE_REACTION_ADD") { onReaction (d["user_id"], d["channel_id"], d["message_id"], d["emoji"]);
				} else if (t == "MESSAGE_REACTION_REMOVE") { onDeleteReaction (d["user_id"], d["channel_id"], d["message_id"], d["emoji"]);
				} else if (t == "MESSAGE_REACTION_REMOVE_ALL") { onDeleteAllReaction (d["guild_id"], d["channel_id"], d["message_id"]);
				} else {
					setError(EVENT_UNKNOWN);
					onError(ERROR_NOTE, t);
				}
				onDispatch(d);
				break;
			case HELLO:
				heartbeatInterval = d["heartbeat_interval"].get<int>();
				heartbeat();
				if (!ready) sendIdentity();
				else sendResume();
				if (reconnectTimer.isValid())
					reconnectTimer.stop();
				break;
			case RECONNECT:
				reconnect();
				break;
			case INVALID_SESSION:
				if (d.get<bool>() == true) {
					schedule(&BaseDiscordClient::sendResume, 2500);
				} else {
					sessionID = "";
					schedule(&BaseDiscordClient::scheduledSendIdentity, 2500);
				}
				break;
			case HEARTBEAT_ACK:
				wasHeartbeatAcked = true;
				onHeartbeatAck();
				break;
			}
		} catch (std::exception& e) {
			std::cerr << "[\033[31mERROR\033[0m] Failed to process message " << t << ": " << e.what() << "\n";
		}
	}

	void BaseDiscordClient::processCloseCode(const int16_t code) {
		setError(code);

		switch (code) {
		//Just reconnect
		case 1006:
		case UNKNOWN_ERROR:
		case UNKNOWN_OPCODE:
		case DECODE_ERROR:
		case NOT_AUTHENTICATED:
		case ALREADY_AUTHENTICATED:
		case INVALID_SEQ:
		case RATE_LIMITED:
		case SESSION_TIMEOUT:
		default:
			break;

		case 1000:
			if (!isQuiting()) {
				return quit(false, true);
				break;
			}
			//else fall through

		//Might be Unrecoveralbe
		//We may need to stop to prevent a restart loop.
		case AUTHENTICATION_FAILED:
		case INVALID_SHARD:
		case SHARDING_REQUIRED:
			return quit(false, true);
			break;
		}
		reconnect(1001);
	}

	void BaseDiscordClient::heartbeat() {
		if (heartbeatInterval <= 0 || isQuiting()) return; //sanity test

		//if time and timer are out of sync, trust time
		time_t currentTime = getEpochTimeMillisecond();
		time_t nextHeartbest;
		if (currentTime < (nextHeartbest = lastHeartbeat + heartbeatInterval)) {
			heart = schedule(&BaseDiscordClient::heartbeat, nextHeartbest - currentTime);
			return;
		}

		if (!wasHeartbeatAcked) {
			reconnect(1001);
		} else {
			sendHeartbeat();
		}

		lastHeartbeat = currentTime;

		heart = schedule(&BaseDiscordClient::heartbeat, heartbeatInterval);
	}

	void BaseDiscordClient::sendHeartbeat() {
		sendL(json{
			{"op", 1},
			{"d", lastSReceived}
		}.dump());
		wasHeartbeatAcked = false;
		onHeartbeat();
	}

	//
	//Voice
	//

#if USE_VOICE

	VoiceContext& BaseDiscordClient::createVoiceContext(Snowflake<Server> server, Snowflake<Channel> channel, BaseVoiceEventHandler * eventHandler) {
		Snowflake<Server> serverTarget = server != "" ? server : getChannel(channel).cast().serverID;
		voiceContexts.push_front({ serverTarget, channel, eventHandler });
		waitingVoiceContexts.emplace_front(&voiceContexts.front());
		return voiceContexts.front();
	}

	void BaseDiscordClient::connectToVoiceChannel(VoiceContext& voiceContext, VoiceMode settings) {
		sendL(json{
			{"op", 4},
			{"d", {
				{"guild_id", voiceContext.serverID},
				{"channel_id", voiceContext.channelID},
				{"self_mute", settings & mute},
				{"self_deaf", settings & deafen}
			}}
		}.dump());
		// Discord will response by sending a VOICE_STATE_UPDATE and a VOICE_SERVER_UPDATE payload. Take a look at processMessage function at case VOICE_STATE_UPDATE and voiceServerUpdate
	}

	VoiceContext& BaseDiscordClient::connectToVoiceChannel(Snowflake<Server> server, Snowflake<Channel> channel, VoiceMode settings) {
		VoiceContext& target = createVoiceContext(server, channel);
		connectToVoiceChannel(target, settings);
		return target;
	}

	void BaseDiscordClient::connectToVoiceIfReady(VoiceContext& context) {
		if (context.endpoint == "" || context.sessionID == "") //check that we are ready
			return;

		//remove the port numbers at the end of the endpoint string
		std::string& givenEndpoint = context.endpoint;
		givenEndpoint = givenEndpoint.substr(0, givenEndpoint.find(':'));

		std::string endpoint;
		//Discord doens't gives the endpoint with wss:// or ?v=3, so it's done here
		//length of wss:///?v=3 is 11, plus one equals 12
		endpoint.reserve(12 + givenEndpoint.length());
		endpoint += "wss://";
		endpoint += givenEndpoint;
		endpoint += "/?v=3";

		//Add a new connection to the list of connections
		voiceConnections.emplace_front( this, context );
		VoiceConnection& voiceConnection = voiceConnections.front();

		connect(endpoint, &voiceConnection, voiceConnection.connection);

		//remove from wait list
		waitingVoiceContexts.remove(&context);
	}

	void BaseDiscordClient::removeVoiceConnectionAndContext(VoiceConnection & connection) {
		const VoiceContext& context = connection.getContext();
		voiceConnections.remove_if(
			[&connection](VoiceConnection& right) {
				return connection == right;
			}
		);
		voiceContexts.remove(context);
	}

#endif

	const time_t BaseDiscordClient::getEpochTimeMillisecond() {
		auto ms = std::chrono::time_point_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now());
		return ms.time_since_epoch().count();
	}

	const std::string BaseDiscordClient::getEditPositionString(const std::vector<std::pair<std::string, size_t>>& positions) {
		std::vector<std::string> params(positions.size());
		for (auto& value : positions) {
			params.push_back(json{
				{"id", value.first},
				{"position", (unsigned) value.second}
			}.dump());
		}
		return json(params).dump();
	}

	Route::Route(const std::string route, const std::initializer_list<std::string>& _values)
		: path(route), values(_values)
	{
		constexpr char channelIDIdentifier[] = "channel.id";
		constexpr char serverIDIdentifier[] = "guild.id";

		size_t targetSize = path.length();
		for (std::string replacement : values)
			targetSize += replacement.length();
		_url.reserve(targetSize);

		//In the future, use string view

		size_t offset = 0;
		for (std::string replacement : values) {
			const size_t start = path.find('{', offset);
			const size_t end = path.find('}', start);

			//the +1 and -1 removes the { and }
			const std::string identifier = path.substr(start + 1, end - start - 1);

			if (identifier == channelIDIdentifier)
				channelID = replacement;
			else if (identifier == serverIDIdentifier)
				serverID = replacement;

			_url += path.substr(offset, start - offset);
			_url += replacement;
			offset = end + 1; //the +1 removes the }
		}
		_url += path.substr(offset, path.length() - offset);

	}

	Route::Route(const char* route) : Route(route, {}) {}

	const std::string Route::bucket(RequestMethod method) {
		std::string target;
		std::string methodString = std::to_string(method);
		target.reserve(methodString.length() + channelID.string().length()
			+ serverID.string().length() + path.length());
		target += methodString;
		target += channelID;
		target += serverID;
		target += path;
		return target;
	}
}
