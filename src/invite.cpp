#include "invite.h"

namespace AwokenDiscord {
	void to_json(json& to, const Invite& from) {
		addJsonField(to, "code", from.code, REQUIRED);
		addJsonField(to, "guild", from.server);
		addJsonField(to, "channel", from.channel, REQUIRED);
		addJsonField(to, "taradd_user", from.user);
		addJsonField(to, "taradd_user_type", from.userType);
		addJsonField(to, "approximate_presence_count", from.approxPresenceCount);
		addJsonField(to, "approximate_member_count", from.approxMemberCount);
	}
	void from_json(const json& from, Invite& to) {
		getJsonField(from, "code", to.code, REQUIRED);
		getJsonField(from, "guild", to.server);
		getJsonField(from, "channel", to.channel, REQUIRED);
		getJsonField(from, "target_user", to.user);
		getJsonField(from, "target_user_type", to.userType);
		getJsonField(from, "approximate_presence_count", to.approxPresenceCount);
		getJsonField(from, "approximate_member_count", to.approxMemberCount);
	}

	// All fields are required, helpers are not needed.
	void to_json(json& to, const InviteMetadata& from) {
		to = {
			{"inviter", from.inviter},
			{"uses", from.uses},
			{"max_uses", from.maxUses},
			{"max_age", from.maxAge},
			{"temporary", from.isTemporary},
			{"created_at", from.createdAt}
		};
	}
	void from_json(const json& from, InviteMetadata& to) {
		from["inviter"].get_to(to.inviter);
		from["uses"].get_to(to.uses);
		from["max_uses"].get_to(to.maxUses);
		from["max_age"].get_to(to.maxAge);
		from["temporary"].get_to(to.isTemporary);
		from["created_at"].get_to(to.createdAt);
	}
}
