#include "attachment.h"

namespace AwokenDiscord {
	void to_json(json& to, const Attachment& from) {
		to = json{
			{"id", from.ID},
			{"filename", from.filename},
			{"size", from.size},
			{"url", from.url},
			{"proxy_url", from.proxyUrl}
		};
		addJsonField(to, "height", from.height, NULLABLE);
		addJsonField(to, "width", from.width, NULLABLE);
	}
	void from_json(const json& from, Attachment& to) {
		from["id"].get_to(to.ID);
		from["filename"].get_to(to.filename);
		from["size"].get_to(to.size);
		from["url"].get_to(to.url);
		from["proxy_url"].get_to(to.proxyUrl);
		getJsonField(from, "height", to.height, NULLABLE);
		getJsonField(from, "width", to.width, NULLABLE);
	}
}