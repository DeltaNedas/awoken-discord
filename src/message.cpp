#include "message.h"
#include "client.h"

namespace AwokenDiscord {
	bool Message::wasPinged(Snowflake<User> userID) {
		size_t size = pingedUsers.size();
		for (size_t i = 0; i < size; i++)
			if (pingedUsers[i].ID == userID) return true;
		return false;
	}

	bool Message::wasPinged(User& user) {
		return wasPinged(user.ID);
	}

	Message Message::send(BaseDiscordClient* client) {
		return client->sendMessage(channelID, content, !embeds.empty() ? embeds[0] : Embed(), tts);
	}

	Message Message::reply(BaseDiscordClient* client, std::string content, Embed embed, bool tts) {
		return client->sendMessage(channelID, content, embed, tts);
	}


	void to_json(json& to, const MessageApplication& from) {
		addJsonField(to, "id", from.ID, REQUIRED);
		addJsonField(to, "cover_image", from.cover);
		addJsonField(to, "description", from.description, REQUIRED);
		addJsonField(to, "icon", from.icon, NULLABLE);
		addJsonField(to, "name", from.name, REQUIRED);
	}
	void from_json(const json& from, MessageApplication& to) {
		getJsonField(from, "id", to.ID, REQUIRED);
		getJsonField(from, "cover_image", to.cover);
		getJsonField(from, "description", to.description, REQUIRED);
		getJsonField(from, "icon", to.icon, NULLABLE);
		getJsonField(from, "name", to.name, REQUIRED);
	}

	void to_json(json& to, const MessageOrigin& from) {
		addJsonField(to, "message_id", from.ID);
		addJsonField(to, "channel_id", from.channelID);
		addJsonField(to, "guild_id", from.serverID);
	}
	void from_json(const json& from, MessageOrigin& to) {
		getJsonField(from, "message_id", to.ID);
		getJsonField(from, "channel_id", to.channelID);
		getJsonField(from, "guild_id", to.serverID);
	}

	void to_json(json& to, const Message& from) {
		addJsonField(to, "id", from.ID, REQUIRED);
		addJsonField(to, "channel_id", from.channelID, REQUIRED);
		addJsonField(to, "guild_id", from.serverID);
		addJsonField(to, "author", from.author, REQUIRED);
		addJsonField(to, "member", from.member);
		addJsonField(to, "content", from.content, REQUIRED);
		addJsonField(to, "timestamp", from.timestamp, REQUIRED);
		addJsonField(to, "edited_timestamp", from.editedAt, NULLABLE);
		addJsonField(to, "tts", from.tts, REQUIRED);
		addJsonField(to, "mention_everyone", from.pingedEveryone, REQUIRED);
		addJsonField(to, "mentions", from.pingedUsers, REQUIRED);
		addJsonField(to, "mention_roles", from.pingedRoles, REQUIRED);
		addJsonField(to, "mention_channels", from.pingedChannels);
		addJsonField(to, "attachments", from.attachments, REQUIRED);
		addJsonField(to, "embeds", from.embeds, REQUIRED);
		addJsonField(to, "reactions", from.reactions);
		addJsonField(to, "nonce", from.nonce, NULLABLE);
		addJsonField(to, "pinned", from.pinned, REQUIRED);
		addJsonField(to, "webhook_id", from.webhookID);
		addJsonField(to, "type", fromEnum(from.type), REQUIRED);
		addJsonField(to, "activity", fromEnum(from.activity));
		addJsonField(to, "application", from.application);
		addJsonField(to, "message_reference", from.origin);
		addJsonField(to, "flags", fromEnum(from.flags));
	}
	void from_json(const json& from, Message& to) {
		long type = 0, flags = 0, activity = 0;
		getJsonField(from, "id", to.ID, REQUIRED);
		getJsonField(from, "channel_id", to.channelID, REQUIRED);
		getJsonField(from, "guild_id", to.serverID);
		getJsonField(from, "author", to.author, REQUIRED);
		getJsonField(from, "member", to.member);
		getJsonField(from, "content", to.content, REQUIRED);
		getJsonField(from, "timestamp", to.timestamp, REQUIRED);
		getJsonField(from, "edited_timestamp", to.editedAt, NULLABLE);
		getJsonField(from, "tts", to.tts, REQUIRED);
		getJsonField(from, "mention_everyone", to.pingedEveryone, REQUIRED);
		getJsonField(from, "mentions", to.pingedUsers, REQUIRED);
		getJsonField(from, "mention_roles", to.pingedRoles, REQUIRED);
		getJsonField(from, "mention_channels", to.pingedChannels);
		getJsonField(from, "attachments", to.attachments, REQUIRED);
		getJsonField(from, "embeds", to.embeds, REQUIRED);
		getJsonField(from, "reactions", to.reactions);
		getJsonField(from, "nonce", to.nonce, NULLABLE);
		getJsonField(from, "pinned", to.pinned, REQUIRED);
		getJsonField(from, "webhook_id", to.webhookID);
		getJsonField(from, "type", type, REQUIRED);
		getJsonField(from, "activity", activity);
		getJsonField(from, "application", to.application);
		getJsonField(from, "message_reference", to.origin);
		getJsonField(from, "flags", flags);
		to.flags = toEnum<Message::MessageFlags>(flags);
		to.activity = toEnum<Message::MessageActivityType>(activity);
	}

	void to_json(json& to, const CreateMessageParams& from) {
		to = {
			{"channel_id", from.channelID},
			{"content", from.content},
			{"embed", from.embed},
			{"tts", from.tts}
		};
	}
	void from_json(const json& from, CreateMessageParams& to) {
		from["channel_id"].get_to(to.channelID);
		from["content"].get_to(to.content);
		from["embed"].get_to(to.embed);
		from["tts"].get_to(to.tts);
	}
}