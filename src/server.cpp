#include "server.h"
#include "client.h"
#include "permissions.h"

namespace AwokenDiscord {
	std::list<ServerMember>::iterator Server::findMember(Snowflake<User> userID) {
		return userID.findObject(members.begin(), members.end());
	}

	std::list<Channel>::iterator Server::findChannel(Snowflake<Channel> channelID) {
		return channelID.findObject(channels.begin(), channels.end());
	}

	std::list<Role>::iterator Server::findRole(Snowflake<Role> roleID) {
		return roleID.findObject(roles.begin(), roles.end());
	}


	void to_json(json& to, const ServerMember& from) {
		addJsonField(to, "user", from.user, REQUIRED);
		addJsonField(to, "nick", from.nick);
		addJsonField(to, "roles", from.roles, REQUIRED);
		addJsonField(to, "joined_at", from.joinedAt, REQUIRED);
		addJsonField(to, "premium_since", from.boostedAt, OPTIONAL_NULLABLE);
		addJsonField(to, "deaf", from.deaf, REQUIRED);
		addJsonField(to, "mute", from.mute, REQUIRED);
	}
	void from_json(const json& from, ServerMember& to) {
		getJsonField(from, "user", to.user); // Not required because Message.member does not include it
		getJsonField(from, "nick", to.nick, OPTIONAL_NULLABLE);
		getJsonField(from, "roles", to.roles, REQUIRED);
		getJsonField(from, "joined_at", to.joinedAt, REQUIRED);
		getJsonField(from, "premium_since", to.boostedAt, OPTIONAL_NULLABLE);
		getJsonField(from, "deaf", to.deaf, REQUIRED);
		getJsonField(from, "mute", to.mute, REQUIRED);
		to.ID = to.user.ID;
	}

	void to_json(json& to, const ServerEmbed& from) {
		addJsonField(to, "enabled", from.enabled, REQUIRED);
		addJsonField(to, "channel_id", from.channelID, NULLABLE);
	}
	void from_json(const json& from, ServerEmbed& to) {
		getJsonField(from, "enabled", to.enabled, REQUIRED);
		getJsonField(from, "channel_id", to.channelID, NULLABLE);
	}

	void to_json(json& to, const Server& from) {
		addJsonField(to, "id", from.ID, REQUIRED);
		addJsonField(to, "name", from.name, REQUIRED);
		addJsonField(to, "icon", from.icon, NULLABLE);
		addJsonField(to, "splash", from.splash, NULLABLE);
		addJsonField(to, "owner", from.isOwner);
		addJsonField(to, "owner_id", from.ownerID, REQUIRED);
		addJsonField(to, "permissions", fromEnum(from.permissions));
		addJsonField(to, "region", from.region, REQUIRED);
		addJsonField(to, "afk_channel_id", from.afkChannelID, NULLABLE);
		addJsonField(to, "afk_timeout", from.afkTimeout, REQUIRED);
		addJsonField(to, "embed_enabled", from.embedEnabled);
		addJsonField(to, "embed_channel_id", from.embedChannelID, OPTIONAL_NULLABLE);
		addJsonField(to, "verification_level", from.verificationLevel, REQUIRED);
		addJsonField(to, "default_message_notifications", from.defaultMessageNotifications, REQUIRED);
		addJsonField(to, "explicit_content_filter", from.explicitContentFilter, REQUIRED);
		addJsonField(to, "roles", from.roles, REQUIRED);
		addJsonField(to, "emojis", from.emojis, REQUIRED);
		addJsonField(to, "features", from.features, REQUIRED);
		addJsonField(to, "mfa_level", from.mfaLevel, REQUIRED);
		addJsonField(to, "application_id", from.applicationID, NULLABLE);
		addJsonField(to, "widget_enabled", from.widgetEnabled);
		addJsonField(to, "widget_channel_id", from.widgetChannelID);
		addJsonField(to, "system_channel_id", from.systemChannelID, NULLABLE);
		addJsonField(to, "joined_at", from.joinedAt);
		addJsonField(to, "large", from.large);
		addJsonField(to, "voice_states", from.voiceStates);
		addJsonField(to, "members", from.members);
		addJsonField(to, "channels", from.channels);
		addJsonField(to, "presences", from.presences);
		addJsonField(to, "max_presences", from.maxPresences, OPTIONAL_NULLABLE);
		addJsonField(to, "max_members", from.maxMembers);
		addJsonField(to, "vanity_url_code", from.vanityInvite, NULLABLE);
		addJsonField(to, "description", from.description, NULLABLE);
		addJsonField(to, "banner", from.banner, NULLABLE);
		addJsonField(to, "premium_tier", from.boostTier, REQUIRED);
		addJsonField(to, "premium_subscription_count", from.boosters);
		addJsonField(to, "preferred_locale", from.preferredLocale, REQUIRED);
	}
	void from_json(const json& from, Server& to) {
		long permissions;
		getJsonField(from, "id", to.ID, REQUIRED);
		getJsonField(from, "name", to.name, REQUIRED);
		getJsonField(from, "icon", to.icon, NULLABLE);
		getJsonField(from, "splash", to.splash, NULLABLE);
		getJsonField(from, "owner", to.isOwner);
		getJsonField(from, "owner_id", to.ownerID, REQUIRED);
		getJsonField(from, "permissions", permissions);
		getJsonField(from, "region", to.region, REQUIRED);
		getJsonField(from, "afk_channel_id", to.afkChannelID, NULLABLE);
		getJsonField(from, "afk_timeout", to.afkTimeout, REQUIRED);
		getJsonField(from, "embed_enabled", to.embedEnabled);
		getJsonField(from, "embed_channel_id", to.embedChannelID, OPTIONAL_NULLABLE);
		getJsonField(from, "verification_level", to.verificationLevel, REQUIRED);
		getJsonField(from, "default_message_notifications", to.defaultMessageNotifications, REQUIRED);
		getJsonField(from, "explicit_content_filter", to.explicitContentFilter, REQUIRED);
		getJsonField(from, "roles", to.roles, REQUIRED);
		getJsonField(from, "emojis", to.emojis, REQUIRED);
		getJsonField(from, "features", to.features, REQUIRED);
		getJsonField(from, "mfa_level", to.mfaLevel, REQUIRED);
		getJsonField(from, "application_id", to.applicationID, NULLABLE);
		getJsonField(from, "widget_enabled", to.widgetEnabled);
		getJsonField(from, "widget_channel_id", to.widgetChannelID, NULLABLE);
		getJsonField(from, "system_channel_id", to.systemChannelID, NULLABLE);
		getJsonField(from, "joined_at", to.joinedAt);
		getJsonField(from, "large", to.large);
		getJsonField(from, "voice_states", to.voiceStates);
		getJsonField(from, "members", to.members);
		getJsonField(from, "channels", to.channels);
		getJsonField(from, "presences", to.presences);
		getJsonField(from, "max_presences", to.maxPresences, OPTIONAL_NULLABLE);
		getJsonField(from, "max_members", to.maxMembers);
		getJsonField(from, "vanity_url_code", to.vanityInvite, NULLABLE);
		getJsonField(from, "description", to.description, NULLABLE);
		getJsonField(from, "banner", to.banner, NULLABLE);
		getJsonField(from, "premium_tier", to.boostTier, REQUIRED);
		getJsonField(from, "premium_subscription_count", to.boosters, NULLABLE);
		getJsonField(from, "preferred_locale", to.preferredLocale, REQUIRED);
		to.permissions = toEnum<Permission>(permissions);
	}
}