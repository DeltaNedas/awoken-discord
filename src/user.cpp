#include "user.h"

namespace AwokenDiscord {
	void to_json(json& to, const User& from) {
		addJsonField(to, "id", from.ID, REQUIRED);
		addJsonField(to, "username", from.username);
		addJsonField(to, "discriminator", from.discriminator);
		addJsonField(to, "avatar", from.avatar, OPTIONAL_NULLABLE);
		addJsonField(to, "bot", from.bot);
		addJsonField(to, "mfa_enabled", from.mfaEnabled);
		addJsonField(to, "verified", from.verified);
		addJsonField(to, "email", from.email, OPTIONAL_NULLABLE);
		addJsonField(to, "flags", from.flags);
		addJsonField(to, "premium_type", from.premiumType);
	}
	void from_json(const json& from, User& to) {
		getJsonField(from, "id", to.ID, REQUIRED);
		getJsonField(from, "username", to.username);
		getJsonField(from, "discriminator", to.discriminator);
		getJsonField(from, "avatar", to.avatar, OPTIONAL_NULLABLE);
		getJsonField(from, "bot", to.bot);
		getJsonField(from, "mfa_enabled", to.mfaEnabled);
		getJsonField(from, "verified", to.verified);
		getJsonField(from, "email", to.email, OPTIONAL_NULLABLE);
		getJsonField(from, "flags", to.flags);
		getJsonField(from, "premium_type", to.premiumType);
	}

	void to_json(json& to, const Connection& from) {
		to = {
			{"id", from.ID},
			{"name", from.name},
			{"type", from.type},
			{"revoked", from.revoked}
		};
	}
	void from_json(const json& from, Connection& to) {
		from["id"].get_to(to.ID);
		from["name"].get_to(to.name);
		from["type"].get_to(to.type);
		from["revoked"].get_to(to.revoked);
	}
}