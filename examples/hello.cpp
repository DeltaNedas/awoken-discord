#include <awoken_discord/awoken_discord.h>
#include <iostream>

class MyClientClass : public AwokenDiscord::DiscordClient {
public:
	using AwokenDiscord::DiscordClient::DiscordClient;
	void onReady(AwokenDiscord::Ready readyData) override { // readyData is server list and stuff
		std::cout << "Bot is ready.\n";
	}
	void onMessage(AwokenDiscord::Message message) override {
		std::cout << "Message sent by " << message.author.username << ": " << message.content << "\n";
		if (message.content.rfind("!hello", 0) == 0) {
			AwokenDiscord::Embed embed;
			embed.title = "Hello!";
			embed.description = "Hello, **" + message.author.username + "**!";
			sendMessage(message.channelID, "", embed);
		}
	}
	// Add more stuff here, see src/default_functions.cpp for everything you can override!
};

int main() {
	std::cout << "Please enter your bot's token: ";
	std::string token = "";
	std::cin >> token;
	MyClientClass client(token, AwokenDiscord::USER_CONTROLLED_THREADS);
	client.run();
}
