# Awoken Discord
C++ library for Discord

# Why?
Make > CMake

# How to build?
```
git clone https://bitbucket.org/DeltaNedas/awoken-discord --recursive
cd awoken-discord/cpr
mkdir build; cd build
cmake -E env CXXFLAGS="-fPIC" cmake ..
make
cd ../..
make -j$(nproc) USE_ASIO=1
sudo make install
```

# Example
*For hello.cpp*
Input: Message received
```
!hello world
```
Possible Output: Message sent
```
Hello DeltaNedas
```
See more examples in the examples directory.

# Will Updating the library break my bot?
Yes, and for now I don't plan on making 0.0 versions backwards compatible with 1.0 versions or later.

# Requirements
* [OpenSSL](https://www.openssl.org/)
* [cpr](https://github.com/whoshuu/cpr)
* [Websocket++](https://github.com/zaphoyd/websocketpp)
* GNU/Linux or BSD (untested) system.

# Master branch
See master branch if you don't want this library to break too often.
